{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Arrow      ((|||))
import           System.Environment

import           FOL
import           LCF
import           Parser
import           Types

testLcftaut :: IO ()
testLcftaut = do
  s:_ <- getArgs
  p (parseForm s >>= lcftaut :: Result Form)

testLcfrefute :: IO ()
testLcfrefute = do
  s:_ <- getArgs
  p (parseForm s >>= \fm -> lcfrefute fm 1 simpcont :: Result Form)

testLcffol :: IO ()
testLcffol = do
  s:_ <- getArgs
  p (parseForm s >>= lcffol :: Result Form)

testProve1 :: IO ()
testProve1 = p (doProve :: Result Form)
  where
    doProve = do
      fm <- parseForm $ "(forall x. x <= x) /\\ (forall x y z. x <= y /\\ y <= z ==> x <= z) /\\ " ++
                        "(forall x y. f(x) <= y <=> x <= g(y)) ==> " ++
                        "(forall x y. x <= y ==> f(x) <= f(y)) /\\ (forall x y. x <= y ==> g(x) <= g(y))"
      prove fm [ impIntroTac "ant"
               , conjIntroTac
               , autoTac by ["ant"]
               , autoTac by ["ant"]
               ]

ewd954 :: Result Form
ewd954 = do
  fm <- parseForm $ "(forall x y. x <= y <=> x * y = x) /\\ (forall x y. f(x * y) = f(x) * f(y)) " ++
                    "==> forall x y. x <= y ==> f(x) <= f(y)"
  lm1 <- parseForm "forall x y. x = y ==> y = x"
  lm2 <- parseForm "forall x y z. x = y /\\ y = z ==> x = z"
  lm3 <- parseForm "forall x y. x = y ==> f(x) = f(y)"
  lm4 <- parseForm "forall x y. x <= y <=> x * y = x"
  lm5 <- parseForm "forall x y. f(x * y) = f(x) * f(y)"
  lm6 <- parseForm "x <= y"
  lm7 <- parseForm "x * y = x"
  lm8 <- parseForm "f(x * y) = f(x)"
  lm9 <- parseForm "f(x) = f(x * y)"
  lm10 <- parseForm "f(x) = f(x) * f(y)"
  lm11 <- parseForm "f(x) * f(y) = f(x)"
  lm12 <- parseForm "f(x) <= f(y)"
  x <- parseTerm "x"
  y <- parseTerm "y"
  z <- parseTerm "z"
  th1 <- eqSym "x" "y"
  th2 <- eqTrans "x" "y" "z"
  let th3 = axiomFuncong "f" ["x"] ["y"]
  prove fm [ note "eq_sym" lm1 using [th1]
           , note "eq_trans" lm2 using [th2]
           , note "eq_cong" lm3 using [th3]
           , assume [("le", lm4), ("hom", lm5)]
           , fix "x"
           , fix "y"
           , assume [("xy", lm6)]
           , so have lm7 by ["le"]
           , so have lm8 by ["eq_cong"]
           , so have lm9 by ["eq_sym"]
           , so have lm10 by ["eq_trans", "hom"]
           , so have lm11 by ["eq_sym"]
           , so conclude lm12 by ["le"]
           , qed
           ]

testProve2 :: IO ()
testProve2 = p ewd954

main :: IO ()
main = testProve2

must :: Result a -> a
must = error ||| id

p :: Show a => Result a -> IO ()
p = putStrLn ||| print
