{-# LANGUAGE ScopedTypeVariables #-}
module LCF where

import           Control.Arrow        (second, (***))
import           Control.Monad        (join, mplus, zipWithM)
import           Control.Monad.Except
import           Data.Either          (fromRight)
import           Data.Foldable        (foldrM)
import           Data.List            (elemIndex, findIndex, sortBy)
import qualified Data.Map             as M
import           Data.Maybe           (catMaybes, fromMaybe)
import           Data.Ord             (comparing)
import qualified Data.Set             as S

import           FOL
import           Types

type Result = Either String

-- Instantiation and skolemization formula
type ES = (Term -> Term, Form)

class Proofsystem thm where
  -- (p => q) => p => q
  modusponens :: thm -> thm -> Result thm
  -- p => forall x. p
  gen :: String -> thm -> thm
  -- p => q => p
  axiomAddimp :: Form -> Form -> thm
  -- (p => q => r) => (p => q) => (p => r)
  axiomDistribimp :: Form -> Form -> Form -> thm
  -- ((p => false) => false) => p
  axiomDoubleneg :: Form -> thm
  -- (forall x. p => q) => (forall x. p) => (forall x. q)
  axiomAllimp :: String -> Form -> Form -> thm
  -- p => forall x. p if x \not_in fv(p)
  axiomImpall :: String -> Form -> Result thm
  -- exists x. x = t if x \not_in fvt(t)
  axiomExistseq :: String -> Term -> Result thm
  -- t = t
  axiomEqrefl :: Term -> thm
  -- s1 = t1 => ... => sn = tn => f(s1, ..., sn) = f(t1, ..., tn)
  axiomFuncong :: String -> [Term] -> [Term] -> thm
  -- s1 = t1 => ... => sn = tn => p(s1, ..., sn) = p(t1, ..., tn)
  axiomPredcong :: String -> [Term] -> [Term] -> thm
  -- (p <=> q) => p => q
  axiomIffimp1 :: Form -> Form -> thm
  -- (p <=> q) => q => p
  axiomIffimp2 :: Form -> Form -> thm
  -- (p => q) => (q => p) => (p <=> q)
  axiomImpiff :: Form -> Form -> thm
  -- true <=> (false => false)
  axiomTrue :: thm
  -- ~p <=> (p => false)
  axiomNot :: Form -> thm
  -- p /\ q <=> ((p => q => false) => false)
  axiomAnd :: Form -> Form -> thm
  -- p \/ q <=> ~(~p /\ ~q)
  axiomOr :: Form -> Form -> thm
  -- (exists x. p) <=> ~(forall x. ~p)
  axiomExists :: String -> Form -> thm
  concl :: thm -> Form

instance Proofsystem Form where
  modusponens (Imp p' q) p | p == p' = Right q
  modusponens x y          = Left $ "modusponens: " ++ show (x, y)
  gen = Forall
  axiomAddimp p q = Imp p $ Imp q p
  axiomDistribimp p q r = Imp (Imp p (Imp q r)) (Imp (Imp p q) (Imp p r))
  axiomDoubleneg p = Imp (Imp (Imp p Bottom) Bottom) p
  axiomAllimp x p q = Imp (Forall x (Imp p q)) (Imp (Forall x p) (Forall x q))
  axiomImpall x p | not (freeIn (Var x) p) = Right $ Imp p $ Forall x p
                  | otherwise = Left $ "axiomImpall: " ++ x ++ " " ++ show p
  axiomExistseq x t | not (occursIn (Var x) t) = Right $ Exists x $ mkEq (Var x) t
                    | otherwise = Left $ "axiomExistseq: " ++ show (x, t)
  axiomEqrefl t = mkEq t t
  axiomFuncong f xs ys = foldr Imp (mkEq (Fn f xs) (Fn f ys)) $ zipWith mkEq xs ys
  axiomPredcong p xs ys = foldr Imp (Imp (Atom (R p xs)) (Atom (R p ys))) $ zipWith mkEq xs ys
  axiomIffimp1 p q = Imp (Iff p q) (Imp p q)
  axiomIffimp2 p q = Imp (Iff p q) (Imp q p)
  axiomImpiff p q = Imp (Imp p q) (Imp (Imp q p) (Iff p q))
  axiomTrue = Iff Top (Imp Bottom Bottom)
  axiomNot p = Iff (Not p) (Imp p Bottom)
  axiomAnd p q = Iff (And p q) (Imp (Imp p (Imp q Bottom)) Bottom)
  axiomOr p q = Iff (Or p q) (Not (And (Not p) (Not q)))
  axiomExists x p = Iff (Exists x p) (Not (Forall x (Not p)))
  concl = id

destEq :: Form -> Result (Term, Term)
destEq (Atom (R "=" [s, t])) = Right (s, t)
destEq p                     = Left $ "destEq: " ++ show p

antecedent :: Form -> Result Form
antecedent (Imp p _) = Right p
antecedent p         = Left $ "antecedent: " ++ show p

consequent :: Form -> Result Form
consequent (Imp _ p) = Right p
consequent p         = Left $ "consequent: " ++ show p

-- p => p
impRefl :: Proofsystem thm => Form -> Result thm
impRefl p = do
  q <- modusponens (axiomDistribimp p (Imp p p) p) (axiomAddimp p (Imp p p))
  modusponens q (axiomAddimp p p)

-- p => p => q |- p => q
impUnduplicate :: Proofsystem thm => thm -> Result thm
impUnduplicate th = case concl th of
  Imp p (Imp p' q) | p == p' -> do
    r <- modusponens (axiomDistribimp p p q) th
    s <- impRefl p
    modusponens r s
  _ -> Left $ "impUnduplicate: pattern mismatch: " ++ show (concl th)

negatef :: Form -> Form
negatef (Imp p Bottom) = p
negatef p              = Imp p Bottom

negativef :: Form -> Bool
negativef (Imp p Bottom) = True
negativef _              = False

addAssum :: Proofsystem thm => Form -> thm -> Result thm
addAssum p th = modusponens (axiomAddimp (concl th) p) th

-- q => r |- (p => q) => p => r
impAddAssum :: Proofsystem thm => Form -> thm -> Result thm
impAddAssum p th = case concl th of
  Imp q r -> do
    th1 <- addAssum p th
    let th2 = axiomDistribimp p q r
    modusponens th2 th1
  _ -> Left $ "impAddAssum: pattern mismatch: " ++ show (concl th)

-- p => q, q => r |- p => r
impTrans :: Proofsystem thm => thm -> thm -> Result thm
impTrans th1 th2 = case (concl th1, concl th2) of
  (Imp p q, Imp q' r) | q == q' -> impAddAssum p th2 >>= flip modusponens th1
  _ -> Left $ "impTrans: pattern mismatch: " ++ show (concl th1, concl th2)

-- p => r, q => r |- p => r
impInsert :: Proofsystem thm => Form -> thm -> Result thm
impInsert q th = case concl th of
  Imp p r -> impTrans th $ axiomAddimp r q
  _       -> Left $ "impInsert: pattern mismatch:" ++ show (q, concl th)

-- p => q => r |- q => p => r
impSwap :: Proofsystem thm => thm -> Result thm
impSwap th = case concl th of
  Imp p (Imp q r) -> modusponens (axiomDistribimp p q r) th >>= impTrans (axiomAddimp q p)
  _               -> Left $ "impSwap: pattern mismatch:" ++ show (concl th)

-- (q => r) => (p => q) => p => r
impTransTh :: Proofsystem thm => Form -> Form -> Form -> Result thm
impTransTh p q r = impTrans (axiomAddimp (Imp q r) p) (axiomDistribimp p q r)

-- p => q |- (q => r) => p => r
impAddConcl :: Proofsystem thm => Form -> thm -> Result thm
impAddConcl r th = case concl th of
  Imp p q -> do
    th1 <- impTransTh p q r >>= impSwap
    modusponens th1 th
  _ -> Left $ "impAddConcl: " ++ show (r, concl th)

-- (p => q => r) => q => p => r
impSwapTh :: Proofsystem thm => Form -> Form -> Form -> Result thm
impSwapTh p q r = do
  th <- impAddConcl (Imp p r) (axiomAddimp q p)
  impTrans (axiomDistribimp p q r) th

-- ((p => q => r) => s => t => u) |- (q => p => r) => t => s => u
impSwap2 :: Proofsystem thm => thm -> Result thm
impSwap2 th = case concl th of
  Imp (Imp p (Imp q r)) (Imp s (Imp t u)) -> do
    th1 <- impSwapTh q p r
    th2 <- impSwapTh s t u
    th3 <- impTrans th th2
    impTrans th1 th3
  _ -> Left $ "impSwap2: " ++ show (concl th)

-- p => q => r, p => q |- p => r
rightMp :: Proofsystem thm => thm -> thm -> Result thm
rightMp ith th = impSwap ith >>= impTrans th >>= impUnduplicate

-- p <=> q |- p => q
iffImp1 :: Proofsystem thm => thm -> Result thm
iffImp1 th = case concl th of
  Iff p q -> modusponens (axiomIffimp1 p q) th
  _       -> Left $ "iffImp1: " ++ show (concl th)

-- p <=> q |- q => p
iffImp2 :: Proofsystem thm => thm -> Result thm
iffImp2 th = case concl th of
  Iff p q -> modusponens (axiomIffimp2 p q) th
  _       -> Left $ "iffImp2: " ++ show (concl th)

-- p => q, q => p |- p <=> q
impAntisym :: Proofsystem thm => thm -> thm -> Result thm
impAntisym th1 th2 = case concl th1 of
  Imp p q -> do
    th3 <- modusponens (axiomImpiff p q) th1
    modusponens th3 th2
  _ -> Left $ "impAntisym: " ++ show (concl th1, concl th2)

-- p => (q => false) => false |- p => q
rightDoubleneg :: Proofsystem thm => thm -> Result thm
rightDoubleneg th = case concl th of
  Imp _ (Imp (Imp p Bottom) Bottom) -> impTrans th $ axiomDoubleneg p
  _                                 -> Left $ "rightDoubleneg: " ++ show (concl th)

-- false => p
exFalso :: Proofsystem thm => Form -> Result thm
exFalso p = rightDoubleneg $ axiomAddimp Bottom $ Imp p Bottom

-- p => q => r, r => s |- p => q => s
impTrans2 :: Proofsystem thm => thm -> thm -> Result thm
impTrans2 th1 th2 = case (concl th1, concl th2) of
  (Imp p (Imp q r), Imp r' s) | r == r' -> do
    th3 <- impTransTh q r s
    th4 <- modusponens th3 th2
    th5 <- impAddAssum p th4
    modusponens th5 th1
  _ -> Left $ "impTrans2: " ++ show (concl th1, concl th2)

-- p => q1, ..., p => qn, q1 => ... => qn => r |- p => r
impTransChain :: Proofsystem thm => [thm] -> thm -> Result thm
impTransChain [] _ = Left "impTransChain: no theorem in 1st argument"
impTransChain (hd:tl) th = do
  th1 <- impTrans hd th
  foldrM (\a b -> impSwap b >>= impTrans a >>= impUnduplicate) th1 (reverse tl)

-- (q => false) => p => (p => q) => false
impTruefalse :: Proofsystem thm => Form -> Form -> Result thm
impTruefalse p q = do
  th1 <- impTransTh p q Bottom
  th2 <- impSwapTh (Imp p q) p Bottom
  impTrans th1 th2

-- (p' => p) => (q => q') => (p => q) => p' => q'
impMonoTh :: Proofsystem thm => Form -> Form -> Form -> Form -> Result thm
impMonoTh p p' q q' = do
  th1 <- impTransTh (Imp p q) (Imp p' q) (Imp p' q')
  th2 <- impTransTh p' q q'
  th3 <- impTransTh p' p q >>= impSwap
  impTrans th2 th1 >>= impSwap >>= impTrans th3

-- true
truth :: Proofsystem thm => Result thm
truth = do
  th1 <- iffImp2 axiomTrue
  th2 <- impRefl Bottom
  modusponens th1 th2

-- p => q |- ~q => ~p
contrapos :: Proofsystem thm => thm -> Result thm
contrapos th = case concl th of
  Imp p q -> do
    th1 <- iffImp1 $ axiomNot q
    th2 <- impAddConcl Bottom th
    th3 <- iffImp2 $ axiomNot p
    impTrans th1 th2 >>= flip impTrans th3
  _ -> Left $ "contrapos: " ++ show (concl th)

-- p /\ q => p
andLeft :: Proofsystem thm => Form -> Form -> Result thm
andLeft p q = do
  th1 <- iffImp1 $ axiomAnd p q
  impAddAssum p (axiomAddimp Bottom q) >>= impAddConcl Bottom >>= rightDoubleneg >>= impTrans th1

-- p /\ q => q
andRight :: Proofsystem thm => Form -> Form -> Result thm
andRight p q = do
  th1 <- iffImp1 $ axiomAnd p q
  impAddConcl Bottom (axiomAddimp (Imp q Bottom) p) >>= rightDoubleneg >>= impTrans th1

-- [ p1 /\ ... /\ pn => pi | 1 <= i <= n ]
conjths :: Proofsystem thm => Form -> Result [thm]
conjths fm = conjths' `mplus` sequence [impRefl fm]
  where
    conjths' = case fm of
      And p q -> do
        th1 <- andRight p q
        ths <- conjths q
        (:) <$> andLeft p q <*> mapM (impTrans th1) ths
      _ -> Left "conjth': patterm mismatch"

-- p => q => p /\ q
andPair :: Proofsystem thm => Form -> Form -> Result thm
andPair p q = do
  th1 <- iffImp2 $ axiomAnd p q
  th2 <- impSwapTh (Imp p (Imp q Bottom)) q Bottom
  th3 <- impTrans2 th2 th1 >>= impAddAssum p
  impRefl (Imp p (Imp q Bottom)) >>= impSwap >>= modusponens th3

-- p /\ q => r |- p => q => r
shunt :: Proofsystem thm => thm -> Result thm
shunt th = case concl th of
  Imp (And p q) _ -> do
    th1 <- andPair p q
    impAddAssum q th >>= impAddAssum p >>= flip modusponens th1
  _ -> Left $ "shunt: pattern mismatch: " ++ show (concl th)

-- p => q => r |- p /\ q => r
unshunt :: Proofsystem thm => thm -> Result thm
unshunt th = case concl th of
  Imp p (Imp q _) -> sequence [andLeft p q, andRight p q] >>= flip impTransChain th
  _ -> Left $ "unshunt: pattern mismatch: " ++ show (concl th)

-- (p <=> q) <=> (p => q) /\ (q => p)
iffDef :: Proofsystem thm => Form -> Form -> Result thm
iffDef p q = do
  th1 <- andPair (Imp p q) (Imp q p)
  let ths = [axiomIffimp1 p q, axiomIffimp2 p q]
  th2 <- impTransChain ths th1
  th3 <- unshunt $ axiomImpiff p q
  impAntisym th2 th3

expandConnective :: Proofsystem thm => Form -> Result thm
expandConnective fm = case fm of
  Top        -> Right axiomTrue
  Not p      -> Right $ axiomNot p
  And p q    -> Right $ axiomAnd p q
  Or p q     -> Right $ axiomOr p q
  Iff p q    -> iffDef p q
  Exists x p -> Right $ axiomExists x p
  _          -> Left $ "expandConnective: pattern mismatch " ++ show fm

eliminateConnective :: Proofsystem thm => Form -> Result thm
eliminateConnective fm
  | not (negativef fm) = expandConnective fm >>= iffImp1
  | otherwise          = expandConnective (negatef fm) >>= iffImp2 >>= impAddConcl Bottom

-- ((p => q) => false) => p; ((p => q) => false) => q => false
impFalseConseqs :: Proofsystem thm => Form -> Form -> Result [thm]
impFalseConseqs p q = sequence
  [ exFalso q >>= impAddAssum p >>= impAddConcl Bottom >>= rightDoubleneg
  , impRefl q >>= impInsert p >>= impAddConcl Bottom
  ]

-- p => (q => false) => r |- ((p => q) => false) => r
impFalseRule :: Proofsystem thm => thm -> Result thm
impFalseRule th = case concl th of
  Imp p (Imp (Imp r _) _) -> impFalseConseqs p r >>= flip impTransChain th
  _                       -> Left $ "impFalseRule: " ++ show (concl th)

-- ((p => false) => r) => (q => r) => (p => q) => r
impTrueRule :: Proofsystem thm => thm -> thm -> Result thm
impTrueRule th1 th2 = case (concl th1, concl th2) of
  (Imp (Imp p Bottom) r, Imp q r') | r == r' -> do
    th3 <- impAddConcl Bottom th1 >>= rightDoubleneg
    th4 <- impAddConcl Bottom th2
    th5 <- impTruefalse p q >>= impSwap
    th6 <- impTransChain [th3, th4] th5 >>= impAddConcl Bottom
    th7 <- impRefl (Imp (Imp p q) Bottom) >>= impSwap
    impTrans th7 th6 >>= rightDoubleneg
  _ -> Left $ "impTrueRule: " ++ show (concl th1, concl th2)

-- p => ~p => q
impContr :: Proofsystem thm => Form -> Form -> Result thm
impContr p q
  | negativef p = exFalso q >>= impAddAssum (negatef p)
  | otherwise   = exFalso q >>= impAddAssum p >>= impSwap

-- (p1 => ... => pn-1 => pn) => pn => p1 => ... => pn-1
impFrontTh :: Proofsystem thm => Int -> Form -> Result thm
impFrontTh 0 fm = impRefl fm
impFrontTh n fm = case fm of
  Imp p qr -> do
    th1 <- impFrontTh (n - 1) qr >>= impAddAssum p
    case concl th1 of
      Imp _ (Imp _ (Imp q r)) -> impSwapTh p q r >>= impTrans th1
      _ -> Left $ "impFrontTh: logic error: " ++ show fm
  _ -> Left $ "impFrontTh: " ++ show fm

-- p1 => ... => pi => ... => pn |- pi => p1 => ... => pn
impFront :: Proofsystem thm => Int -> thm -> Result thm
impFront n th = impFrontTh n (concl th) >>= flip modusponens th

lcfptab :: Proofsystem thm => [Form] -> [Form] -> Result thm
lcfptab fms lits = case fms of
  Bottom : fl                  -> exFalso $ foldl Imp Bottom $ fl ++ lits
  fm @ (Imp p q) : fl | p == q -> lcfptab fl lits >>= addAssum fm
  Imp (Imp p q) Bottom : fl    -> lcfptab (p : Imp q Bottom : fl) lits >>= impFalseRule
  Imp p q : fl | q /= Bottom   -> join $ impTrueRule <$> lcfptab (Imp p Bottom : fl) lits <*> lcfptab (q : fl) lits
  p @ (Atom _) : fl                  -> lcfptab1 p fl
  p @ (Forall _ _) : fl              -> lcfptab1 p fl
  p @ (Imp (Atom _) Bottom) : fl     -> lcfptab1 p fl
  p @ (Imp (Forall _ _) Bottom) : fl -> lcfptab1 p fl
  fm : fl -> do
    th <- eliminateConnective fm
    case concl th of
      Imp _ q -> lcfptab (q : fl) lits >>= impTrans th
      _       -> Left $ "lcfptab: pattern mismatch: " ++ show (concl th)
  _ -> Left "lcfptab: no contradiction"
  where
    lcfptab1 p fl
      | negatef p `elem` lits =
      case break (== negatef p) lits of
        (l1, _:l2) -> do
          th <- impContr p $ foldr Imp Bottom l2
          foldrM impInsert th $ fl ++ l1
        _ -> Left $ "lcfptab1: pattern mismatch: " ++ show (p, fl)
      | otherwise = lcfptab fl (p : lits) >>= impFront (length fl)

lcftaut :: Proofsystem thm => Form -> Result thm
lcftaut p = lcfptab [negatef p] [] >>= modusponens (axiomDoubleneg p)

-- s = t ==> t = s
eqSym :: Proofsystem thm => Term -> Term -> Result thm
eqSym s t = f (axiomPredcong "=" [s, s] [t, s]) >>= f
  where
    f th = impSwap th >>= flip modusponens (axiomEqrefl s)

-- s = t ==> t = u ==> s = u
eqTrans :: Proofsystem thm => Term -> Term -> Term -> Result thm
eqTrans s t u = do
  let th1 = axiomPredcong "=" [t, u] [s, u]
  th2 <- impSwap th1 >>= flip modusponens (axiomEqrefl u)
  eqSym s t >>= flip impTrans th2

-- s = t => tm[s] = tm[t]
icongruence :: Proofsystem thm => Term -> Term -> Term -> Term -> Result thm
icongruence s t stm ttm
  | stm == ttm = addAssum (mkEq s t) (axiomEqrefl stm)
  | stm == s && ttm == t = impRefl $ mkEq s t
  | otherwise = case (stm, ttm) of
    (Fn fs sa, Fn ft ta) | fs == ft && length sa == length ta -> do
      ths <- zipWithM (icongruence s t) sa ta
      ts <- mapM (consequent . concl) ths
      (ls, rs) <- mapAndUnzipM destEq ts
      impTransChain ths $ axiomFuncong fs ls rs
    _ -> Left $ "icongruence: Pattern mismatch: " ++ show (s, t, stm, ttm)

-- (forall x. p => Q[x]) => p => (forall x. Q[x])
genRightTh :: Proofsystem thm => String -> Form -> Form -> Result thm
genRightTh x p q = do
  th1 <- axiomImpall x p
  th2 <- impSwap $ axiomAllimp x p q
  impTrans th1 th2 >>= impSwap

-- P[x] => Q[x] |- (forall x. P[x]) => (forall x. Q[x])
genimp :: Proofsystem thm => String -> thm -> Result thm
genimp x th = case concl th of
  Imp p q -> modusponens (axiomAllimp x p q) (gen x th)
  _       -> Left $ "genimp: " ++ show (concl th)

-- p => (forall x. Q[x])
genRight :: Proofsystem thm => String -> thm -> Result thm
genRight x th = case concl th of
  Imp p q -> genRightTh x p q >>= flip modusponens (gen x th)
  _       -> Left $ "genRight: " ++ show (concl th)

-- forall x. P[x] => q => (exists x. P[x]) => q
existsLeftTh :: Proofsystem thm => String -> Form -> Form -> Result thm
existsLeftTh x p q = do
  th1 <- impTransTh p q Bottom >>= impSwap >>= genimp x
  th2 <- genRightTh x q' p' >>= impTrans th1
  th3 <- impTransTh q' (Forall x p') Bottom >>= impSwap
  th4 <- impTrans th2 th3 >>= flip impTrans2 (axiomDoubleneg q)
  th5 <- iffImp2 (axiomNot p) >>= genimp x >>= impAddConcl Bottom
  th6 <- iffImp1 (axiomNot (Forall x (Not p))) >>= flip impTrans th5
  th7 <- iffImp1 (axiomExists x p) >>= flip impTrans th6
  impSwap th4 >>= impTrans th7 >>= impSwap
  where
    p' = Imp p Bottom
    q' = Imp q Bottom

-- P[x] => q |- (exists x. P[x]) => q
existsLeft :: Proofsystem thm => String -> thm -> Result thm
existsLeft x th = case concl th of
  Imp p q -> existsLeftTh x p q >>= flip modusponens (gen x th)
  _       -> Left $ "existsLeft: Pattern mismatch: " ++ show (x, concl th)

-- x = t => P[x] => P[t] |- (forall x. P[x]) => P[t]
subspec :: Proofsystem thm => thm -> Result thm
subspec th = case concl th of
  Imp (e @ (Atom (R "=" [Var x, t]))) (Imp p q) -> do
    th1 <- existsLeftTh x e q
    th2 <- impSwap th >>= genimp x >>= flip impTrans th1
    th3 <- axiomExistseq x t
    impSwap th2 >>= flip modusponens th3
  _ -> Left $ "subspec: Pattern mismatch: " ++ show (concl th)

-- x = t => P[x] => P[t] |- (forall x. P[x]) => (forall t. P[t])
subalpha :: Proofsystem thm => thm -> Result thm
subalpha th = case concl th of
  Imp (Atom (R "=" [Var x, Var y])) (Imp p q)
    | x == y -> modusponens th (axiomEqrefl (Var x)) >>= genimp x
    | otherwise -> subspec th >>= genRight y
  _ -> Left $ "subalpha: Pattern mismatch: " ++ show (concl th)

-- s = t => P[s] => P[t]
isubst :: Proofsystem thm => Term -> Term -> Form -> Form -> Result thm
isubst s t sfm tfm
  | sfm == tfm = impRefl tfm >>= addAssum (mkEq s t)
  | otherwise = case (sfm, tfm) of
    (Atom (R p sa), Atom (R p' ta)) | p == p' && length sa == length ta -> do
      ths <- zipWithM (icongruence s t) sa ta
      (ls, rs) <- mapAndUnzipM (join . fmap destEq . consequent . concl) ths
      impTransChain ths $ axiomPredcong p ls rs
    (Imp sp sq, Imp tp tq) -> do
      th1 <- eqSym s t
      th2 <- isubst t s tp sp >>= impTrans th1
      th3 <- isubst s t sq tq
      impMonoTh sp tp sq tq >>= impTransChain [th2, th3]
    (Forall x p, Forall y q)
      | x == y -> do
        th1 <- isubst s t p q
        genRight x th1 >>= flip impTrans (axiomAllimp x p q)
      | otherwise -> do
        let z = Var $ variant x $ uniq $ join [fv p, fv q, fvt s, fvt t]
        th1 <- isubst (Var x) z p $ subst (M.singleton x z) p
        th2 <- isubst z (Var y) (subst (M.singleton y z) q) q
        th3 <- subalpha th1
        th4 <- subalpha th2
        th5 <- join $ isubst s t <$> consequent (concl th3) <*> antecedent (concl th4)
        impSwap th5 >>= impTrans th3 >>= flip impTrans2 th4 >>= impSwap
    _ -> do
      sth <- expandConnective sfm >>= iffImp1
      tth <- expandConnective tfm >>= iffImp2
      th1 <- join $ isubst s t <$> consequent (concl sth) <*> antecedent (concl tth)
      impTrans2 th1 tth >>= impSwap >>= impTrans sth >>= impSwap

-- (forall x. F[x]) => (forall z. F[z])
alpha :: Proofsystem thm => String -> Form -> Result thm
alpha z fm = case fm of
  Forall x p -> do
    let p' = subst (M.singleton x (Var z)) p
    isubst (Var x) (Var z) p p' >>= subalpha
  _ -> Left $ "alpha: Pattern mismatch: " ++ show (z, fm)

-- (forall x. F[x]) => F[z]
ispec :: Proofsystem thm => Term -> Form -> Result thm
ispec t fm = case fm of
  Forall x p
    | x `elem` fvt t -> do
      th <- flip alpha fm $ variant x $ uniq $ fvt t ++ var p
      consequent (concl th) >>= ispec t >>= impTrans th
    | otherwise -> isubst (Var x) t p (subst (M.singleton x t) p) >>= subspec
  _ -> Left $ "ispec: Pattern mismatch: " ++ show (t, fm)

-- forall x. F[x] |- F[z]
spec :: Proofsystem thm => Term -> thm -> Result thm
spec t th = ispec t (concl th) >>= flip modusponens th

unifyComplementsf :: Env -> Form -> Form -> Maybe Env
unifyComplementsf env fm1 fm2 = case (fm1, fm2) of
  (Atom (R p1 a1), Imp (Atom (R p2 a2)) Bottom) -> unify env [(Fn p1 a1, Fn p2 a2)]
  (Imp (Atom (R p1 a1)) Bottom, Atom (R p2 a2)) -> unify env [(Fn p1 a1, Fn p2 a2)]
  _ -> Nothing

-- ((q => f) => ... => (q => p) => r) => (p => f) => ... => (q => p) => r
useLaterimp :: Proofsystem thm => Form -> Form -> Result thm
useLaterimp i fm = case fm of
  Imp (Imp q' s) (Imp (i' @ (Imp q p)) r) | i' == i -> do
    let th1 = axiomDistribimp i (Imp (Imp q s) r) (Imp (Imp p s) r)
    th2 <- impTransTh q p s >>= impSwap
    th3 <- impTransTh (Imp p s) (Imp q s) r >>= impSwap
    impTrans th2 th3 >>= modusponens th1 >>= impSwap2
  Imp qs (Imp a b) -> useLaterimp i (Imp qs b) >>= impAddAssum a >>= impSwap2
  _ -> Left $ "useLaterimp: Pattern mismatch: " ++ show (i, fm)

-- p => (q => false) => r |- ((p => q) => false) => r
impFalseRule' :: Proofsystem thm => (ES -> Result thm) -> ES -> Result thm
impFalseRule' th es = th es >>= impFalseRule

-- ((p => false) => r) => (q => r) => (p => q) => r
impTrueRule' :: Proofsystem thm => (ES -> Result thm) -> (ES -> Result thm) -> ES -> Result thm
impTrueRule' th1 th2 es = join $ impTrueRule <$> th1 es <*> th2 es

-- p1 => ... => pi => ... => pn |- pi => p1 => ... => pn
impFront' :: Proofsystem thm => Int -> (ES -> Result thm) -> ES -> Result thm
impFront' n thp es = thp es >>= impFront n

addAssum' :: Proofsystem thm => Form -> (ES -> Result thm) -> ES -> Result thm
addAssum' fm thp (es @ (e, s)) = thp es >>= addAssum (onformula e fm)

eliminateConnective' :: Proofsystem thm => Form -> (ES -> Result thm) -> ES -> Result thm
eliminateConnective' fm thp (es @ (e, s)) =
  join $ impTrans <$> eliminateConnective (onformula e fm) <*> thp es

spec' :: Proofsystem thm => Term -> Form -> Int -> (ES -> Result thm) -> ES -> Result thm
spec' y fm n thp (es @ (e, s)) = do
  th1 <- ispec (e y) (onformula e fm)
  thp es >>= impFront n >>= impSwap >>= impTrans th1 >>= impUnduplicate

exFalso' :: Proofsystem thm => [Form] -> ES -> Result thm
exFalso' fms (e, s) = exFalso $ foldr (Imp . onformula e) s fms

complits' :: Proofsystem thm => ([Form], [Form]) -> Int -> ES -> Result thm
complits' ([], _) _ _ = Left "complits': Pattern mismatch"
complits' (p : fl, lits) i (e, s) =
  case splitAt i lits of
    (_, []) -> Left "complits': Pattern mismatch"
    (l1, p' : l2) -> do
      let fm = foldr (Imp . onformula e) s l2
      th1 <- impContr (onformula e p) fm
      foldrM (impInsert . onformula e) th1 $ fl ++ l1

deskol' :: Proofsystem thm => Form -> (ES -> Result thm) -> ES -> Result thm
deskol' skh thp (es @ (e, s)) = do
  th <- thp es
  useLaterimp (onformula e skh) (concl th) >>= flip modusponens th

lcftab :: forall thm. Proofsystem thm
       => (Form -> Maybe Term) -> [Form] -> [Form] -> Int
       -> ((ES -> Result thm) -> (Env, [(Form, Term)], Int) -> Result thm)
       -> (Env, [(Form, Term)], Int) -> Result thm
lcftab skofun fms lits n cont (ek @ (env, sks, k))
  | n < 0 = Left "lcftab: No proof"
  | otherwise = case fms of
    Bottom : fl -> cont (exFalso' $ fl ++ lits) ek
    (fm @ (Imp p q)) : fl | p == q ->
      lcftab skofun fl lits n (cont . addAssum' fm) ek
    Imp (Imp p q) Bottom : fl ->
      lcftab skofun (p : Imp q Bottom : fl) lits n (cont . impFalseRule') ek
    Imp p q : fl | q /= Bottom ->
      lcftab skofun (Imp p Bottom : fl) lits n (\th -> lcftab skofun (q : fl) lits n (cont . impTrueRule' th)) ek
    (p @ (Atom _)) : fl -> lcftab1 p fl
    (p @ (Imp (Atom _) Bottom)) : fl -> lcftab1 p fl
    (fm @ (Forall x p)) : fl -> do
      let y = Var ("X_" ++ show k)
      lcftab skofun (subst (M.singleton x y) p : fl ++ [fm]) lits (n - 1) (cont . spec' y fm (length fms)) (env, sks, k + 1)
    Imp (yp @ (Forall y p)) Bottom : fl ->
      case skofun yp of
        Nothing -> Left $ "lcftab: Failed to skolemize: " ++ show yp
        Just fx -> do
          let p' = subst (M.singleton y fx) p
          let skh = Imp p' (Forall y p)
          let sks' = (Forall y p, fx) : sks
          lcftab skofun (Imp p' Bottom : fl) lits n (cont . deskol' skh) (env, sks', k)
    fm : fl -> do
      fm' <- (eliminateConnective fm :: Result thm) >>= consequent . concl
      lcftab skofun (fm' : fl) lits n (cont . eliminateConnective' fm) ek
    [] -> Left "lcftab: No contradiction"
  where
    lcftab1 p fl =
      msum (map (\p' ->
        case unifyComplementsf env p p' of
          Just env' -> cont (\es -> index p' lits >>= \i -> complits' (fms, lits) i es) (env', sks, k)
          Nothing -> Left "lcftab1") lits)
        `mplus` lcftab skofun fl (p : lits) n (cont . impFront' (length fl)) ek
    index x ls = case elemIndex x ls of
      Just i  -> Right i
      Nothing -> Left "index"

quantforms' :: Bool -> Form -> S.Set Form
quantforms' e fm = case fm of
  Not p      -> quantforms' (not e) p
  And p q    -> quantforms' e p `S.union` quantforms' e q
  Or p q     -> quantforms' e p `S.union` quantforms' e q
  Imp p q    -> quantforms' e $ Or (Not p) q
  Iff p q    -> quantforms' e $ Or (And p q) (And (Not p) (Not q))
  Exists x p -> if e then S.insert fm (quantforms' e p) else quantforms' e p
  Forall x p -> if e then quantforms' e p else S.insert fm (quantforms' e p)
  _          -> S.empty

quantforms :: Bool -> Form -> [Form]
quantforms e fm = S.toList $ quantforms' e fm

skolemfuns :: Form -> [(Form, Term)]
skolemfuns fm = catMaybes $ zipWith skofun [1..] skts
  where
    fns = map fst $ getFuncs fm
    skts = map (\fm' -> case fm' of Exists x p -> Forall x (Not p); _ -> fm') $ quantforms True fm
    skofun i ap = case ap of
      Forall y p -> Just $ (,) ap $ Fn (variant ("f_" ++ show i) fns) (map Var (fv ap))
      _ -> Nothing

termMatch :: Env -> [(Term, Term)] -> Maybe Env
termMatch env eqs = case eqs of
  [] -> Just env
  (Fn f fa, Fn g ga) : oth | f == g && length fa == length ga ->
    termMatch env $ zip fa ga ++ oth
  (Var x, t) : oth
    | x `M.notMember` env -> termMatch (M.insert x t env) oth
    | M.lookup x env == Just t -> termMatch env oth
    | otherwise -> Nothing

formMatch :: Form -> Form -> Env -> Maybe Env
formMatch f1 f2 env = case (f1, f2) of
  (Bottom, Bottom)               -> Just env
  (Top, Top)                     -> Just env
  (Atom (R p pa), Atom (R q qa)) -> termMatch env [(Fn p pa, Fn q qa)]
  (Not p1, Not p2)               -> formMatch p1 p2 env
  (And p1 q1, And p2 q2)         -> formMatch q1 q2 env >>= formMatch p1 p2
  (Or p1 q1, Or p2 q2)           -> formMatch q1 q2 env >>= formMatch p1 p2
  (Imp p1 q1, Imp p2 q2)         -> formMatch q1 q2 env >>= formMatch p1 p2
  (Iff p1 q1, Iff p2 q2)         -> formMatch q1 q2 env >>= formMatch p1 p2
  (Forall x1 p1, Forall x2 p2)   | x1 == x2 -> formMatch1 x1 p1 x2 p2
  (Exists x1 p1, Exists x2 p2)   | x1 == x2 -> formMatch1 x1 p1 x2 p2
  _                              -> Nothing
  where
    formMatch1 x1 p1 x2 p2 = M.delete z <$> formMatch (instFn p1) (instFn p2) env
      where
        z = variant x1 $ uniq $ fv p1 ++ fv p2
        instFn = subst (M.singleton x1 (Var z))

lcfrefute :: Proofsystem thm
          => Form -> Int
          -> ((ES -> Result thm) -> (Env, [(Form, Term)], Int) -> Result thm)
          -> Result thm
lcfrefute fm n cont = lcftab findSkolem [fm] [] n cont (M.empty, [], 0)
  where
    sfm = skolemfuns fm
    findSkolem fm = msum $ map (\(f, t) -> substT <$> formMatch f fm M.empty <*> pure t) sfm

mkSkol :: (Form, Term) -> Form -> Result Form
mkSkol fm q = case fm of
  (Forall y p, fx) -> Right $ Imp (Imp (subst (M.singleton y fx) p) (Forall y p)) q
  _                -> Left $ "mkSkol: Pattern mismatch: " ++ show fm

simpcont :: Proofsystem thm => (ES -> Result thm) -> (Env, [(Form, Term)], Int) -> Result thm
simpcont thp (env, sks, k) = do
  fm <- onformula ifn <$> foldrM mkSkol Bottom sks
  thp (ifn, fm)
  where
    ifn = substT (solve env)

elimSkolemvar :: Proofsystem thm => thm -> Result thm
elimSkolemvar th = case concl th of
  Imp (Imp pv (apx @ (Forall x px))) q -> do
    [th1, th2] <- impAddConcl Bottom th >>= \th0 -> impFalseConseqs pv apx >>= mapM (impTrans th0)
    let v = head $ sub (fv pv) (fv apx) ++ [x]
    th3 <- genRight v th1
    th4 <- consequent (concl th3) >>= alpha x >>= impTrans th3
    rightMp th2 th4 >>= modusponens (axiomDoubleneg q)
  _ -> Left $ "elimSkolemvar: Pattern mismatch: " ++ show (concl th)
  where
    sub xs ys = S.toList $ S.fromList xs `S.difference` S.fromList ys

replacet :: (Term -> Maybe Term) -> Term -> Term
replacet f tm = case tm of
  Fn g args -> fromMaybe (Fn g (map (replacet f) args)) $ f tm
  Var _     -> fromMaybe tm $ f tm

repeatResult :: (a -> Result a) -> a -> a
repeatResult f x = fromRight x (repeatResult f <$> f x)

deskolcont :: Proofsystem thm => (ES -> Result thm) -> (Env, [(Form, Term)], Int) -> Result thm
deskolcont thp (env, sks, k) =
  fmap (repeatResult (impSwap >=> elimSkolemvar))
    (foldrM mkSkol Bottom ssk >>= thp . (,) (vfn . ifn) . onformula vfn)
  where
    ifn = substT (solve env)
    isk = uniq $ map (onformula ifn *** ifn) sks
    ssk = sortBy (comparing (Prelude.negate . termsize . snd)) isk
    vs = map (Var . ("Y_" ++) . show) [1..]
    vfn = replacet $ flip M.lookup $ M.fromList $ zip (map snd ssk) vs

lcffol :: Proofsystem thm => Form -> Result thm
lcffol fm = do
  th1 <- msum $ map (\n -> lcfrefute fm' n deskolcont) [0..]
  th2 <- modusponens (axiomDoubleneg (negatef fm')) th1
  foldrM (spec . Var) th2 (reverse fvs)
  where
    fvs = fv fm
    fm' = Imp (foldr Forall fm fvs) Bottom

data Goals thm = Goals [([(String, Form)], Form)] ([thm] -> Result thm)

instance Show (Goals thm) where
  showsPrec n (Goals gs f) = foldr (.) id $ zipWith showSubgoal [1..] gs
    where
      showSubgoal i (hyps, gl) =
        showString "[Goal " . shows i . showString "]\n" .
        foldr (.) id (map (\(s, f) -> showString (s ++ ": ") . showsPrec n f . showString"\n") hyps) .
        showString "----\n" .
        showsPrec n gl

setGoal :: forall thm. Proofsystem thm => Form -> Goals thm
setGoal p = Goals gls jfn
  where
    gls = [([], p)]
    jfn :: [thm] -> Result thm
    jfn ths = case ths of
      [th] -> truth >>= modusponens th >>= chk
      _    -> Left ("setGoal: Pattern mismatch: " ++ show (map concl ths))
    chk th = if concl th == p then Right th else Left "setGoal: Wrong theorem"

extractThm :: Goals thm -> Result thm
extractThm gls = case gls of
  Goals [] jfn -> jfn []
  _            -> Left "extractThm: Unsolved goals"

tacProof :: Goals thm -> [Goals thm -> Result (Goals thm)] -> Result thm
tacProof g prf = foldrM ($) g (reverse prf) >>= extractThm

prove :: Proofsystem thm => Form -> [Goals thm -> Result (Goals thm)] -> Result thm
prove p = tacProof (setGoal p)

conjIntroTac :: Proofsystem thm => Goals thm -> Result (Goals thm)
conjIntroTac g = case g of
  Goals ((asl, And p q) : gls) jfn ->
    Right $ Goals ((asl, p) : (asl, q) : gls) (jfn' p q jfn)
  _ -> Left $ "conjIntroTac: Pattern mismatch " ++ show g
  where
    jfn' p q jfn (thp : thq : ths) = andPair p q >>= impTransChain [thp, thq] >>= jfn . (: ths)
    jfn' _ _ _ x                 = Left $ "conjIntroTac: Pattern mismatch " ++ show (map concl x)

jmodify :: ([thm] -> Result thm) -> (thm -> Result thm) -> [thm] -> Result thm
jmodify jfn tfn (th : oths) = tfn th >>= jfn . (: oths)
jmodify _ _ []              = Left "jmodify: Pattern mismatch"

genRightAlpha :: Proofsystem thm => String -> String -> thm -> Result thm
genRightAlpha y x th = do
  th1 <- genRight y th
  consequent (concl th1) >>= alpha x >>= impTrans th1

forallIntroTac :: Proofsystem thm => String -> Goals thm -> Result (Goals thm)
forallIntroTac y g = case g of
  Goals ((asl, fm @ (Forall x p)) : gls) jfn ->
    if y `elem` fv fm || any (elem y . fv . snd) asl
      then Left "forallIntroTac: variable already free in goal"
      else return $ Goals ((asl, subst (M.singleton x (Var y)) p) : gls) (jmodify jfn (genRightAlpha y x))
  _ -> Left $ "forallIntroTac: Pattern mismatch " ++ show g

rightExists :: forall thm. Proofsystem thm => String -> Term -> Form -> Result thm
rightExists x t p = do
  th <- (ispec t (Forall x (Not p)) :: Result thm) >>= contrapos
  case concl th of
    Imp (Not (Not p')) _ -> do
      ths <- sequence [ impContr p' Bottom
                      , iffImp1 (axiomNot p') >>= impAddConcl Bottom
                      , iffImp2 (axiomNot (Not p'))]
      th2 <- iffImp2 (axiomExists x p)
      foldrM impTrans th2 (ths ++ [th])
    _ -> Left $ "rightExists: Pattern mismatch " ++ show (concl th)

existsIntroTac :: Proofsystem thm => Term -> Goals thm -> Result (Goals thm)
existsIntroTac t g = case g of
  Goals ((asl, Exists x p) : gls) jfn ->
    Right $ Goals ((asl, subst (M.singleton x t) p) : gls)
                  (jmodify jfn (\th -> rightExists x t p >>= impTrans th))
  _ -> Left $ "existsIntroTac: Pattern mismatch " ++ show g

impIntroTac :: Proofsystem thm => String -> Goals thm -> Result (Goals thm)
impIntroTac s g = case g of
  Goals ((asl, Imp p q) : gls) jfn ->
    let jmod = if null asl then addAssum Top else shunt >=> impSwap in
    Right $ Goals (((s, p) : asl, q) : gls) (jmodify jfn jmod)
  _ -> Left $ "impIntroTac: Pattern mismatch " ++ show g

assumptate :: Proofsystem thm => Goals thm -> thm -> Result thm
assumptate (Goals ((asl, w) : gls) jfn) th = addAssum (listConj $ map snd asl) th
assumptate g _ = Left $ "assumptate: Pattern mismatch " ++ show g

using :: forall thm a. Proofsystem thm => [thm] -> a -> Goals thm -> Result [thm]
using ths _ g = do
  let ths' = map (\th -> foldr gen th (fv (concl th))) ths :: [thm]
  mapM (assumptate g) ths'

assumps :: forall thm. Proofsystem thm => [(String, Form)] -> Result [(String, thm)]
assumps asl = case asl of
  []       -> return []
  [(l, p)] -> impRefl p >>= \p' -> return [(l, p')]
  (l, p) : lps -> do
    ths <- assumps lps :: Result [(String, thm)]
    case map (second concl) ths of
      (_, Imp q _) : _ -> do
        rth <- andRight p q
        lth <- andLeft p q
        rest <- mapM (\(l, th) -> (,) l <$> impTrans rth th) ths
        return ((l, lth) : rest)

firstassum :: forall thm. Proofsystem thm => [(String, Form)] -> Result thm
firstassum asl = case asl of
  []            -> Left "firstassum: Pattern mismatch"
  [(_, p)]      -> impRefl p
  (_, p) : asl' -> andLeft p $ listConj $ map snd asl'

by :: Proofsystem thm => [String] -> Form -> Goals thm -> Result [thm]
by hyps p g = case g of
  Goals ((asl, w) : gls) jfn -> do
    ths <- assumps asl
    case mapM (`lookup` ths) hyps of
      Just ths' -> Right ths'
      Nothing   -> Left "by: Logic error"
  _ -> Left $ "by: Pattern mismatch " ++ show g

justify :: Proofsystem thm
        => ([a] -> Form -> Goals thm -> Result [thm])
        -> [a] -> Form -> Goals thm -> Result thm
justify byfn hyps p g = do
  ths <- byfn hyps p g
  case map concl ths of
    [fm @ (Imp p' _)] | p == p' -> return (head ths)
    fms -> do
      th <- foldrM (\a b -> consequent a >>= \a' -> return (Imp a' b)) p fms >>= lcffol
      if null ths then assumptate g th else impTransChain ths th

proof :: Proofsystem thm => [Goals thm -> Result (Goals thm)] -> Form -> Goals thm -> Result [thm]
proof tacs p g = case g of
  Goals ((asl, w) : gls) jfn -> sequence [tacProof (Goals [(asl, p)] f) tacs]
  _                          -> Left "proof: Pattern mismatch"
  where
    f xs = case xs of
      []    -> Left "proof: Pattern mismatch"
      (x:_) -> Right x

at once p gl = []

once = []

autoTac :: Proofsystem thm
        => ([String] -> Form -> Goals thm -> Result [thm])
        -> [String] -> Goals thm -> Result (Goals thm)
autoTac byfn hyps g = case g of
  Goals ((asl, w) : gls) jfn -> do
    th <- justify byfn hyps w g
    return $ Goals gls (jfn . (th :))
  _ -> Left "autoTac: Pattern mismatch"

lemmaTac :: Proofsystem thm
         => String -> Form
         -> ([a] -> Form -> Goals thm -> Result [thm])
         -> [a] -> Goals thm -> Result (Goals thm)
lemmaTac s p byfn hyps g = case g of
  Goals ((asl, w) : gls) jfn -> do
    let tr th1 = justify byfn hyps p g >>= \th2 -> impTrans th2 th1
    let mfn = if null asl then tr else shunt >=> tr >=> impUnduplicate
    return $ Goals (((s, p) : asl, w) : gls) (jmodify jfn mfn)
  _ -> Left "lemmaTac: Pattern mismatch"

existsElimTac :: Proofsystem thm
              => String -> Form
              -> ([String] -> Form -> Goals thm -> Result [thm])
              -> [String] -> Goals thm -> Result (Goals thm)
existsElimTac l fm byfn hyps g = case (fm, g) of
  (Exists x p, Goals ((asl, w) : gls) jfn) ->
    if any (elem x . fv) (w : map snd asl)
      then Left "existsElimTac: Variable free in assumptions"
      else do
        th <- justify byfn hyps fm g
        let jfn' pth = shunt pth >>= existsLeft x >>= impTrans th >>= impUnduplicate
        return $ Goals (((l, p) : asl, w) : gls) (jmodify jfn jfn')
  _ -> Left "existsElimTac: Pattern mismatch"

anteDisj :: forall thm. Proofsystem thm => thm -> thm -> Result thm
anteDisj th1 th2 = case (concl th1, concl th2) of
  (Imp p r, Imp q s) -> do
    ths <- mapM contrapos [th1, th2] :: Result [thm]
    th3 <- (andPair (Not p) (Not q) :: Result thm) >>= impTransChain ths
    th4 <- iffImp2 (axiomNot r :: thm) >>= flip impTrans th3 >>= contrapos
    th5 <- iffImp1 (axiomOr p q :: thm) >>= flip impTrans th4
    iffImp1 (axiomNot (Imp r Bottom) :: thm) >>= impTrans th5 >>= rightDoubleneg
  _ -> Left "anteDisj: Pattern mismatch"

disjElimTac :: Proofsystem thm
            => String -> Form
            -> ([String] -> Form -> Goals thm -> Result [thm])
            -> [String] -> Goals thm -> Result (Goals thm)
disjElimTac l fm byfn hyps g = case (fm, g) of
  (Or p q, Goals ((asl, w) : gls) jfn) -> do
    th <- justify byfn hyps fm g
    return $ Goals (((l, p) : asl, w) : ((l, q) : asl, w) : gls) (jfn' th jfn)
  _ -> Left "disjElimTac: Pattern mismatch"
  where
    jfn' th jfn (pth : qth : ths') =
      join (anteDisj <$> shunt pth <*> shunt qth) >>=
        impTrans th >>= impUnduplicate >>= jfn . (: ths')
    jfn' _ _ x = Left $ "disjElimTac: Pattern mismatch " ++ show (map concl x)

funpow :: Monad m => Int -> (a -> m a) -> a -> m a
funpow i f x = if i < 1 then return x else f x >>= funpow (i - 1) f

multishunt :: Proofsystem thm => Int -> thm -> Result thm
multishunt i =
  funpow i (shunt >=> impSwap) >=> impSwap >=> funpow (i - 1) (impFront 2 >=> unshunt) >=> impSwap

assume :: Proofsystem thm => [(String, Form)] -> Goals thm -> Result (Goals thm)
assume lps g = case g of
  Goals ((asl, Imp p q) : gls) jfn ->
    let r = foldr1 And (map snd lps) in
    if r /= p
      then Left $ "assume: Pattern mismatch " ++ show lps
      else
        let jfn' th = if null asl then addAssum Top th else multishunt (length lps) th in
        return $ Goals ((lps ++ asl, q) : gls) (jmodify jfn jfn')
  _ -> Left $ "assume: Pattern mismatch " ++ show g

note :: Proofsystem thm
     => String -> Form
     -> ([a] -> Form -> Goals thm -> Result [thm])
     -> [a] -> Goals thm -> Result (Goals thm)
note = lemmaTac

have :: Proofsystem thm
     => Form
     -> ([String] -> Form -> Goals thm -> Result [thm])
     -> [String] -> Goals thm -> Result (Goals thm)
have p = lemmaTac "" p

so :: Proofsystem thm
   => (Form -> ([a] -> Form -> Goals thm -> Result [thm]) -> [a] -> Goals thm -> Result (Goals thm))
   -> Form -> ([a] -> Form -> Goals thm -> Result [thm]) -> [a] -> Goals thm -> Result (Goals thm)
so tac arg byfn =
  tac arg (\hyps p g -> case g of
    Goals ((asl, w) : _) _ -> (:) <$> firstassum asl <*> byfn hyps p g
    _                      -> Left $ "so: Pattern mismatch" ++ show g)

fix :: Proofsystem thm => String -> Goals thm -> Result (Goals thm)
fix = forallIntroTac

consider :: Proofsystem thm
         => String -> Form
         -> ([String] -> Form -> Goals thm -> Result [thm])
         -> [String] -> Goals thm -> Result (Goals thm)
consider x p = existsElimTac "" $ Exists x p

take :: Proofsystem thm => Term -> Goals thm -> Result (Goals thm)
take = existsIntroTac

cases :: Proofsystem thm
      => Form
      -> ([String] -> Form -> Goals thm -> Result [thm])
      -> [String] -> Goals thm -> Result (Goals thm)
cases = disjElimTac ""

conclude :: Proofsystem thm
         => Form
         -> ([String] -> Form -> Goals thm -> Result [thm])
         -> [String] -> Goals thm -> Result (Goals thm)
conclude p byfn hyps g = case g of
  Goals ((asl, w) : gls) jfn -> do
    th <- justify byfn hyps p g
    if p == w
      then return $ Goals ((asl, Top) : gls) (jmodify jfn (const (return th)))
      else case w of
        And p' q ->
          if p' /= p
            then Left "conclude: bad conclusion"
            else return $ Goals ((asl, q) : gls) (jmodify jfn (mfn p q th))
        _ -> Left "conclude: Pattern mismatch"
  _ -> Left "conclude: Pattern mismatch"
  where
    mfn p q th th' = andPair p q >>= impTransChain [th, th']

qed :: Proofsystem thm => Goals thm -> Result (Goals thm)
qed g = case g of
  Goals ((asl, w) : gls) jfn ->
    if w == Top
      then return $ Goals gls (\ths -> truth >>= assumptate g >>= jfn . (: ths))
      else Left "qed: non-trivial goal"
