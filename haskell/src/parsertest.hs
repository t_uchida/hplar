module Main where

import System.Environment
import Text.Parsec

import Parser

main :: IO ()
main = getArgs >>= \args -> case args of
  []  -> putStrLn "No args"
  h:_ -> case parse form' "" h of
    Left err -> putStrLn $ "Error: " ++ show err
    Right fm -> print fm
