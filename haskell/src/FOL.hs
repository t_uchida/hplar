module FOL where

import           Control.Monad (mplus, msum, (<=<))
import           Data.List     (find, intersect, maximumBy, partition, (\\))
import qualified Data.Map      as M
import           Data.Maybe    (fromMaybe, isJust, listToMaybe)
import           Data.Ord      (comparing)
import qualified Data.Set      as S
import           Prelude       hiding (negate)

import           Types

type Env = M.Map String Term

uniq :: Ord a => [a] -> [a]
uniq = S.elems . S.fromList

onatoms :: (FOL -> FOL) -> Form -> Form
onatoms f fm = case fm of
  Atom a     -> Atom $ f a
  Not p      -> Not $ onatoms f p
  And p q    -> And (onatoms f p) (onatoms f q)
  Or p q     -> Or (onatoms f p) (onatoms f q)
  Imp p q    -> Imp (onatoms f p) (onatoms f q)
  Iff p q    -> Iff (onatoms f p) (onatoms f q)
  Forall x p -> Forall x $ onatoms f p
  Exists x p -> Exists x $ onatoms f p
  _          -> fm

onformula :: (Term -> Term) -> Form -> Form
onformula f = onatoms $ \(R p a) -> R p $ map f a

termsize :: Term -> Int
termsize tm = case tm of
  Var _     -> 1
  Fn f args -> 1 + sum (map termsize args)

occursIn :: Term -> Term -> Bool
occursIn s t
  | s == t = True
  | otherwise = case t of
    Var y     -> False
    Fn _ args -> any (occursIn s) args

freeIn :: Term -> Form -> Bool
freeIn t fm = case fm of
  Bottom          -> False
  Top             -> False
  Atom (R p args) -> any (occursIn t) args
  Not p           -> freeIn t p
  And p q         -> freeIn1 p q
  Or p q          -> freeIn1 p q
  Imp p q         -> freeIn1 p q
  Iff p q         -> freeIn1 p q
  Forall y p      -> freeIn2 y p
  Exists y p      -> freeIn2 y p
  where
    freeIn1 p q = freeIn t p || freeIn t q
    freeIn2 y p = not (occursIn (Var y) t) && freeIn t p

mkEq :: Term -> Term -> Form
mkEq t1 t2 = Atom $ R "=" [t1, t2]

getFuncsT :: Term -> [(String, Int)]
getFuncsT tm = case tm of
  Var _     -> []
  Fn f args -> (f, length args) : concatMap getFuncsT args

getFuncsF :: Form -> [(String, Int)]
getFuncsF fm = case fm of
  Atom (R _ a) -> concatMap getFuncsT a
  Not p        -> getFuncsF p
  And p q      -> getFuncsF p ++ getFuncsF q
  Or p q       -> getFuncsF p ++ getFuncsF q
  Imp p q      -> getFuncsF p ++ getFuncsF q
  Iff p q      -> getFuncsF p ++ getFuncsF q
  Forall _ p   -> getFuncsF p
  Exists _ p   -> getFuncsF p
  _            -> []

getFuncs :: Form -> [(String, Int)]
getFuncs = uniq . getFuncsF

variant :: String -> [String] -> String
variant x vars
  | x `elem` vars = variant (x ++ "'") vars
  | otherwise   = x

fvt :: Term -> [String]
fvt tm = case tm of
  Var x     -> [x]
  Fn _ args -> concatMap fvt args

var' :: Form -> S.Set String
var' fm = case fm of
  Bottom          -> S.empty
  Top             -> S.empty
  Atom (R _ args) -> S.fromList $ concatMap fvt args
  Not p           -> var' p
  And p q         -> var' p `S.union` var' p
  Or p q          -> var' p `S.union` var' p
  Imp p q         -> var' p `S.union` var' p
  Iff p q         -> var' p `S.union` var' p
  Forall x p      -> S.insert x $ var' p
  Exists x p      -> S.insert x $ var' p

var :: Form -> [String]
var = S.elems . var'

fv' :: Form -> S.Set String
fv' fm = case fm of
  Bottom          -> S.empty
  Top             -> S.empty
  Atom (R _ args) -> S.fromList $ concatMap fvt args
  Not p           -> fv' p
  And p q         -> fv' p `S.union` fv' q
  Or p q          -> fv' p `S.union` fv' q
  Imp p q         -> fv' p `S.union` fv' q
  Iff p q         -> fv' p `S.union` fv' q
  Forall x p      -> fv' p `S.difference` S.singleton x
  Exists x p      -> fv' p `S.difference` S.singleton x

fv :: Form -> [String]
fv = S.elems . fv'

substT :: Env -> Term -> Term
substT sfn tm = case tm of
  Var x     -> fromMaybe tm $ M.lookup x sfn
  Fn f args -> Fn f $ map (substT sfn) args

subst :: Env -> Form -> Form
subst subfn fm = case fm of
  Atom (R p args) -> Atom $ R p $ map (substT subfn) args
  Not p           -> Not $ subst subfn p
  And p q         -> And (subst subfn p) (subst subfn q)
  Or p q          -> Or (subst subfn p) (subst subfn q)
  Imp p q         -> Imp (subst subfn p) (subst subfn q)
  Iff p q         -> Iff (subst subfn p) (subst subfn q)
  Forall x p      -> substq subfn Forall x p
  Exists x p      -> substq subfn Exists x p
  _               -> fm

substq :: Env -> (String -> Form -> Form) -> String -> Form -> Form
substq subfn quant x p = quant x' $ subst (M.insert x (Var x') subfn) p
  where
    x' | varExists = variant x (fv (subst (M.delete x subfn) p))
       | otherwise = x
    varExists = any (\y -> x `elem` fvt (fromMaybe (Var y) (M.lookup y subfn))) (fv p \\ [x])

psimplify1 :: Form -> Form
psimplify1 fm = case fm of
  Not Bottom   -> Top
  Not Top      -> Bottom
  Not (Not p)  -> p
  And _ Bottom -> Bottom
  And Bottom _ -> Bottom
  And p Top    -> p
  And Top p    -> p
  Or p Bottom  -> p
  Or Bottom p  -> p
  Imp Bottom _ -> Top
  Imp _ Top    -> Top
  Imp Top p    -> p
  Imp p Bottom -> Not p
  Iff p Top    -> p
  Iff Top p    -> p
  Iff p Bottom -> Not p
  Iff Bottom p -> Not p
  _            -> fm

simplify1 :: Form -> Form
simplify1 fm = case fm of
  Forall x p | x `elem` fv p -> fm | otherwise -> p
  Exists x p | x `elem` fv p -> fm | otherwise -> p
  _          -> psimplify1 fm

simplify :: Form -> Form
simplify fm = case fm of
  Not p      -> simplify1 $ Not $ simplify p
  And p q    -> simplify1 $ And (simplify p) (simplify q)
  Or p q     -> simplify1 $ Or (simplify p) (simplify q)
  Imp p q    -> simplify1 $ Imp (simplify p) (simplify q)
  Iff p q    -> simplify1 $ Iff (simplify p) (simplify q)
  Forall x p -> simplify1 $ Forall x $ simplify p
  Exists x p -> simplify1 $ Exists x $ simplify p
  _          -> fm

nnf :: Form -> Form
nnf fm = case fm of
  And p q          -> And (nnf p) (nnf q)
  Or p q           -> Or (nnf p) (nnf q)
  Imp p q          -> Or (nnf (Not p)) (nnf q)
  Iff p q          -> Or (And (nnf p) (nnf q)) (And (nnf (Not p)) (nnf (Not q)))
  Not (Not p)      -> nnf p
  Not (And p q)    -> Or (nnf (Not p)) (nnf (Not q))
  Not (Or p q)     -> And (nnf (Not p)) (nnf (Not q))
  Not (Imp p q)    -> And (nnf p) (nnf (Not q))
  Not (Iff p q)    -> Or (And (nnf p) (nnf (Not q))) (And (nnf (Not p)) (nnf q))
  Forall x p       -> Forall x (nnf p)
  Exists x p       -> Exists x (nnf p)
  Not (Forall x p) -> Exists x (nnf (Not p))
  Not (Exists x p) -> Forall x (nnf (Not p))
  _ -> fm

pullquants :: Form -> Form
pullquants fm = case fm of
  And (Forall x p) (Forall y q) -> pullq True True fm Forall And x y p q
  Or (Exists x p) (Exists y q)  -> pullq True True fm Exists Or x y p q
  And (Forall x p) q            -> pullq True False fm Forall And x x p q
  And p (Forall y q)            -> pullq False True fm Forall And y y p q
  Or (Forall x p) q             -> pullq True False fm Forall Or x x p q
  Or p (Forall y q)             -> pullq False True fm Forall Or y y p q
  And (Exists x p) q            -> pullq True False fm Exists And x x p q
  And p (Exists y q)            -> pullq False True fm Exists And y y p q
  Or (Exists x p) q             -> pullq True False fm Exists Or x x p q
  Or p (Exists y q)             -> pullq False True fm Exists Or y y p q
  _                             -> fm

pullq :: Bool -> Bool -> Form -> (String -> Form -> Form) -> (Form -> Form -> Form)
      -> String -> String -> Form -> Form -> Form
pullq l r fm quant op x y p q = quant z $ pullquants $ op p' q'
  where
    z = variant x (fv fm)
    p' = if l then subst (M.singleton x (Var z)) p else p
    q' = if r then subst (M.singleton y (Var z)) q else q

prenex :: Form -> Form
prenex fm = case fm of
  Forall x p -> Forall x (prenex p)
  Exists x p -> Exists x (prenex p)
  And p q    -> pullquants (And (prenex p) (prenex q))
  Or p q     -> pullquants (Or (prenex p) (prenex q))
  _          -> fm

pnf :: Form -> Form
pnf = prenex . nnf . simplify

purednf :: Form -> [[Form]]
purednf fm = case fm of
  And p q -> uniq $ (++) <$> purednf p <*> purednf q
  Or p q  -> uniq $ purednf p ++ purednf q
  _       -> [[fm]]

simpdnf :: Form -> [[Form]]
simpdnf fm = case fm of
  Bottom -> []
  Top -> [[]]
  _ -> let djs = filter (not . trivial) $ purednf $ nnf fm in
       filter (\d -> not (any (`sub` d) djs)) djs
  where
    sub xs ys = S.fromList xs `S.isProperSubsetOf` S.fromList ys

purecnf :: Form -> [[Form]]
purecnf = map (map negate) . purednf . nnf . Not

simpcnf :: Form -> [[Form]]
simpcnf fm = case fm of
  Bottom -> [[]]
  Top -> []
  _ -> let cjs = filter (not . trivial) $ purecnf fm in
       filter (\c -> not (any (`sub` c) cjs)) cjs
  where
    sub xs ys = S.fromList xs `S.isProperSubsetOf` S.fromList ys

listDisj :: [Form] -> Form
listDisj [] = Bottom
listDisj l  = foldr1 Or l

listConj :: [Form] -> Form
listConj [] = Top
listConj l  = foldr1 And l

dnf :: Form -> Form
dnf = listDisj . map listConj . simpdnf

skolem :: Form -> [String] -> (Form, [String])
skolem fm fns = case fm of
  Exists y p ->
    let xs = fv fm in
    let f = variant (if null xs then "c_" ++ y else "f_" ++ y) fns in
    let fx = Fn f $ map Var xs in
    skolem (subst (M.singleton y fx) p) (f : fns)
  Forall x p -> let (p', fns') = skolem p fns in (Forall x p', fns')
  And p q -> skolem' And p q fns
  Or p q -> skolem' Or p q fns
  _ -> (fm, fns)

skolem' :: (Form -> Form -> Form) -> Form -> Form -> [String] -> (Form, [String])
skolem' cons p q fns =
  let (p', fns') = skolem p fns in
  let (q', fns'') = skolem q fns' in
  (cons p' q', fns'')

askolemize :: Form -> Form
askolemize fm = fst $ skolem (nnf (simplify fm)) (map fst (getFuncs fm))

specialize :: Form -> Form
specialize fm = case fm of
  Forall _ p -> specialize p
  _          -> fm

skolemize :: Form -> Form
skolemize = specialize . pnf . askolemize

eval :: Form -> (FOL -> Maybe Bool) -> Maybe Bool
eval fm v = case fm of
  Bottom  -> Just False
  Top     -> Just True
  Atom x  -> v x
  Not p   -> not <$> eval p v
  And p q -> (&&) <$> eval p v <*> eval q v
  Or p q  -> (||) <$> eval p v <*> eval q v
  Imp p q -> not <$> ((||) <$> eval p v <*> eval q v)
  Iff p q -> (==) <$> eval p v <*> eval q v
  _       -> Nothing

pholds :: (Form -> Maybe Bool) -> Form -> Maybe Bool
pholds d fm = eval fm (d . Atom)

herbfuns :: Form -> ([(String, Int)], [(String, Int)])
herbfuns fm | null cns  = ([("c", 0)], fns)
            | otherwise = (cns, fns)
  where
    (cns, fns) = partition (\(_, ar) -> ar == 0) $ getFuncs fm

groundtuples :: [Term] -> [(String, Int)] -> Int -> [[Term]]
groundtuples cntms funcs numfv = case funcs of
  [] -> gtuples 0 numfv
  _  -> concatMap (`gtuples` numfv) [0..]
  where
    gterms :: Int -> [Term]
    gterms n = case n of
      0 -> cntms
      _ -> concatMap (\(f, m) -> map (Fn f) (gtuples (n - 1) m)) funcs
    gtuples :: Int -> Int -> [[Term]]
    gtuples n m = case (n, m) of
      (0, 0) -> [[]]
      (_, 0) -> []
      _      -> concatMap (\k -> (:) <$> gterms k <*> gtuples (n - k) (m - 1)) [0..n]

fpf :: Ord a => [a] -> [b] -> M.Map a b
fpf xs ys = M.fromList $ zip xs ys

herbloop :: ([[Form]] -> [[Form]] -> (Form -> Form) -> [[Form]])
         -> ([[Form]] -> Bool)
         -> [[Form]] -> [Term] -> [(String, Int)]
         -> [String] -> Int -> [[Form]] -> Maybe Int
herbloop mfn tfn fl0 cntms funcs fvs n djs = go djs 1 $ take n $ groundtuples cntms funcs $ length fvs
  where
    go _ _ [] = Nothing
    go fl k (t:ts) =
      let fl' = mfn fl0 fl $ subst $ fpf fvs t in
      if not $ tfn fl' then Just k else go fl' (k + 1) ts

positive :: Form -> Bool
positive fm = case fm of Not _ -> False; _ -> True

negative :: Form -> Bool
negative fm = case fm of Not _ -> True; _ -> False

negate :: Form -> Form
negate (Not p) = p
negate p       = Not p

trivial :: [Form] -> Bool
trivial lits = not $ null $ intersect pos $ map negate neg
  where
    (pos, neg) = partition positive lits

generalize :: Form -> Form
generalize fm = foldr Forall fm (fv fm)

gilmoreLoop :: [[Form]] -> [Term] -> [(String, Int)] -> [String] -> Int -> [[Form]] -> Maybe Int
gilmoreLoop = herbloop mfn (not . null)
  where
    mfn djs0 djs ifn = filter (not . trivial) $ map uniq $ (++) <$> map (map ifn) djs0 <*> djs

gilmore :: Form -> Int -> Maybe Int
gilmore fm n = gilmoreLoop (simpdnf sfm) cntms funcs fvs n [[]]
  where
    sfm = skolemize $ Not $ generalize fm
    fvs = fv sfm
    (consts, funcs) = herbfuns sfm
    cntms = map (Var . fst) $ uniq consts

oneLiteralRule :: [[Form]] -> Maybe [[Form]]
oneLiteralRule clauses = do
  u <- find isSingleton clauses >>= listToMaybe
  let u' = negate u
  return . map (sub u') $ filter (notElem u) clauses
  where
    isSingleton l = case l of [_] -> True; _ -> False
    sub y xs = S.elems (S.fromList xs `S.difference` S.singleton y)

affirmativeNegativeRule :: [[Form]] -> Maybe [[Form]]
affirmativeNegativeRule clauses
  | null pures = Nothing
  | otherwise  = Just $ filter (null . intersect pures) clauses
  where
    (neg', pos) = partition negative $ uniq $ concat clauses
    neg = map negate neg'
    posOnly = sub pos neg
    negOnly = sub neg pos
    pures = posOnly ++ map negate negOnly
    sub xs ys = S.elems $ S.fromList xs `S.difference` S.fromList ys

resolveOn :: Form -> [[Form]] -> [[Form]]
resolveOn p clauses = other ++ filter (not . trivial) ((++) <$> pos' <*> neg')
  where
    p' = negate p
    (pos, notpos) = partition (elem p) clauses
    (neg, other) = partition (elem p') notpos
    pos' = map (filter (/= p)) pos
    neg' = map (filter (/= p')) neg

posnegCount :: [[Form]] -> Form -> Int
posnegCount cls l = m + n
  where
    m = length $ filter (elem l) cls
    n = length $ filter (elem (negate l)) cls

dpll :: [[Form]] -> Bool
dpll [] = True
dpll clauses
  | [] `elem` clauses = False
  | otherwise = fromMaybe alt (mplus (dpll <$> oneLiteralRule clauses) (dpll <$> affirmativeNegativeRule clauses))
  where
    p = maximumBy (comparing $ posnegCount clauses) $ filter positive $ concat clauses
    alt = dpll (insert [p] clauses) || dpll (insert [negate p] clauses)
    insert x ys = S.elems $ S.singleton x `S.union` S.fromList ys

dpLoop :: [[Form]] -> [Term] -> [(String, Int)] -> [String] -> Int -> [[Form]] -> Maybe Int
dpLoop = herbloop dp_mfn dpll
  where
    dp_mfn cjs0 cjs ifn = map uniq $ (++) <$> map (map ifn) cjs0 <*> cjs

davisputnum :: Form -> Int -> Maybe Int
davisputnum fm n = dpLoop (simpcnf sfm) cntms funcs fvs n [[]]
  where
    sfm = skolemize $ Not $ generalize fm
    fvs = fv sfm
    (consts, funcs) = herbfuns sfm
    cntms = map (Var . fst) $ uniq consts

istriv :: Env -> String -> Term -> Bool
istriv env x (Var y)     = y == x || fromMaybe False (istriv env x <$> M.lookup y env)
istriv env x (Fn _ args) = any (istriv env x) args

unify :: Env -> [(Term, Term)] -> Maybe Env
unify env eqs = case eqs of
  [] -> Just env
  (Fn f fargs, Fn g gargs) : eqs' ->
    if f == g && length fargs == length gargs
      then unify env $ zip fargs gargs ++ eqs'
      else Nothing
  (Var x, t) : eqs' ->
    if isJust (M.lookup x env)
      then M.lookup x env >>= \s -> unify env ((s, t) : eqs')
      else unify (if istriv env x t then env else M.insert x t env) eqs'
  (t, Var x) : eqs' -> unify env ((Var x, t) : eqs')

solve :: Env -> Env
solve env | env' == env = env
          | otherwise   = solve env'
  where
    env' = M.map (substT env) env

fullunify :: [(Term, Term)] -> Maybe Env
fullunify eqs = solve <$> unify M.empty eqs

unifyLiterals :: Env -> (Form, Form) -> Maybe Env
unifyLiterals env tmp = case tmp of
  (Atom (R p1 a1), Atom (R p2 a2)) -> unify env [(Fn p1 a1, Fn p2 a2)]
  (Not p, Not q)                   -> unifyLiterals env (p, q)
  (Bottom, Bottom)                 -> Just env
  (Top, Top)                       -> Just env
  _                                -> Nothing

unifyComplements :: Env -> (Form, Form) -> Maybe Env
unifyComplements env (p, q) = unifyLiterals env (p, negate q)

unifyRefute :: [[Form]] -> Env -> Maybe Env
unifyRefute djs env = case djs of
  [] -> Just env
  d:dobjs ->
    let (pos, neg) = partition positive d in
    mconcat $ map (unifyRefute dobjs <=< unifyComplements env) $ (,) <$> pos <*> neg

prawitzLoop :: [[Form]] -> [String] -> [[Form]] -> Int -> [Bool]
prawitzLoop djs0 fvs djs n = isJust (unifyRefute djs1 M.empty) : prawitzLoop djs0 fvs djs1 (n + 1)
  where
    l = length fvs
    newvars = map (Var . ('_' :) . show . (+ (n * l))) [1..l]
    inst = fpf fvs newvars
    djs1 = map uniq $ (++) <$> map (map (subst inst)) djs0 <*> djs

prawitz :: Form -> [Bool]
prawitz fm = prawitzLoop (simpdnf fm0) (fv fm0) [[]] 0
  where
    fm0 = skolemize $ Not $ generalize fm

tableau :: [Form] -> [Form] -> Int
        -> ((Env, Int) -> Maybe (Env, Int))
        -> (Env, Int) -> Maybe (Env, Int)
tableau fms lits n cont (env, k)
  | n == 0 = Nothing
  | otherwise = case fms of
      [] -> Nothing
      And p q : unexp -> tableau (p : q : unexp) lits n cont (env, k)
      Or p q : unexp -> tableau (p : unexp) lits n (tableau (q : unexp) lits n cont) (env, k)
      Forall x p : unexp ->
        let y = Var ('_' : show k) in
        let p' = subst (M.singleton x y) p in
        tableau (p' : unexp ++ [Forall x p]) lits (n - 1) cont (env, k + 1)
      fm : unexp -> msum (map (\l -> unifyComplements env (fm, l) >>= \env' -> cont (env', k)) lits)
                    `mplus` tableau unexp (fm:lits) n cont (env, k)

tabrefute :: [Form] -> [Bool]
tabrefute fms = map (\n -> isJust $ tableau fms [] n Just (M.empty, 0)) [0..]

tab :: Form -> [Bool]
tab fm | sfm == Bottom = []
       | otherwise = tabrefute [sfm]
  where
    sfm = askolemize $ Not $ generalize fm
