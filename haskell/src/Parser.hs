module Parser where

import           Control.Arrow          (left)
import           Control.Monad.Identity
import           Data.String
import           Text.Parsec
import           Text.Parsec.Expr
import           Text.Parsec.Language
import qualified Text.Parsec.Token      as T

import           Types

type PM = Parsec String ()

infixr 1 <||>

(<||>) :: PM a -> PM a -> PM a
p1 <||> p2 = try p1 <|> p2

-- Language definition

folOps :: [String]
folOps = ["+", "-", "*", "/"]

compOps :: [String]
compOps = ["=", "<", "<=", ">", ">="]

logOps :: [String]
logOps = ["~", "/\\", "\\/", "==>", "<=>"]

otherOps :: [String]
otherOps = ["^", "::", ".", "(", ")"]

formStyle :: LanguageDef st
formStyle = emptyDef
  { T.identStart = letter <|> char '_'
  , T.identLetter = alphaNum <|> oneOf "_'"
  , T.opStart = op
  , T.opLetter = op
  , T.reservedOpNames = folOps ++ compOps ++ logOps ++ otherOps
  , T.reservedNames = ["true", "false", "forall", "exists"]
  }
  where
    op = oneOf "~`!@#$%^&*-+=|\\:;<>.?/"

-- Tokens

lexer :: T.TokenParser st
lexer = T.makeTokenParser formStyle

reserved :: String -> PM ()
reserved = T.reserved lexer

reservedOp :: String -> PM ()
reservedOp = T.reservedOp lexer

parens :: PM a -> PM a
parens = T.parens lexer

identifier :: PM String
identifier = T.identifier lexer

commaSep :: PM a -> PM [a]
commaSep = T.commaSep lexer

whiteSpace :: PM ()
whiteSpace = T.whiteSpace lexer

-- Operators

table :: OperatorTable String () Identity Form
table = [ [ prefix "~" Not ]
        , [ binary "/\\" And AssocRight ]
        , [ binary "\\/" Or AssocRight ]
        , [ binary "==>" Imp AssocRight ]
        , [ binary "<=>" Iff AssocRight ]
        ]
  where
    binary name fun = Infix (reservedOp name >> return fun)
    prefix name fun = Prefix (reservedOp name >> return fun)

termtable :: OperatorTable String () Identity Term
termtable = [ [ prefix "-" ]
            , [ binary "*" AssocLeft, binary "/" AssocLeft ]
            , [ binary "+" AssocLeft, binary "-" AssocLeft ]
            ]
  where
    binary name = Infix (reservedOp name >> return (\x y -> Fn name [x, y]))
    prefix name = Prefix (reservedOp name >> return (\x -> Fn name [x]))

-- Forms

term :: PM Term
term = buildExpressionParser termtable expr <?> "term expression or function or variable"
  where
    expr = parens term <||> func <||> var <?> "term expression or function or variable"
    func = Fn <$> identifier <*> parens (commaSep term)
    var = Var <$> identifier

atom :: PM Form
atom = do
  x <- term
  rhs "=" x <|> rhs "<=" x <|> rhs "<" x <|> rhs ">=" x <|> rhs ">" x <|> toRel x <?> "operator"
  where
    rhs op x = reservedOp op >> Atom . R op . (\y -> [x, y]) <$> term
    toRel (Var x) = return $ Atom $ R x []
    toRel (Fn f xs) | f `elem` folOps = unexpected f
                    | otherwise     = return $ Atom $ R f xs

form :: PM Form
form = buildExpressionParser table expr <?> "form expression"
  where
    expr = parens form <||> bottom <||> exists <||> forall <||> top <||> atom <?> "form expression"
    forall = reserved "forall" >> flip (foldr Forall) <$> many1 identifier <* reservedOp "." <*> form
    exists = reserved "exists" >> flip (foldr Exists) <$> many1 identifier <* reservedOp "." <*> form
    top = reserved "true" *> return Top
    bottom = reserved "false" *> return Bottom

parseTerm :: String -> Either String Term
parseTerm s = left show $ parse (whiteSpace *> term) "" s

parseForm :: String -> Either String Form
parseForm s = left show $ parse (whiteSpace *> form) "" s

instance IsString Term where
  fromString s = case parseTerm s of
    Left err -> error $ "fromString: Failed to parse '" ++ s ++ "': " ++ err
    Right tm -> tm

instance IsString Form where
  fromString s = case parseForm s of
    Left err -> error $ "fromString: Failed to parse '" ++ s ++ "': " ++ err
    Right fm -> fm
