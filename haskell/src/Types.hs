module Types (
  Form(..),
  Term(..),
  FOL(..)
  )where

import           Data.List (intersperse)

data Form
  = Bottom
  | Top
  | Atom FOL
  | Not Form
  | And Form Form
  | Or Form Form
  | Imp Form Form
  | Iff Form Form
  | Forall String Form
  | Exists String Form
  deriving (Eq, Ord)

data Term
  = Var String
  | Fn String [Term]
  deriving (Eq, Ord)

data FOL
  = R String [Term]
  deriving (Eq, Ord)

instance Show Form where
  showsPrec = showsFormPrec

instance Show Term where
  showsPrec = showsTermPrec

compOps :: [String]
compOps = ["=", "<", "<=", ">", ">="]

data ShowAssoc = ALeft | ARight deriving Eq

genShowBinOp :: (Int -> a -> ShowS) -> Int -> Int -> ShowAssoc -> String -> a -> a -> ShowS
genShowBinOp shw p q assoc f x y = showParen (p > q) $ shw (q + l) x . showString f . shw (q + r) y
  where
    l = if assoc == ARight then 1 else 0
    r = if assoc == ALeft then 1 else 0

showsTermPrec :: Int -> Term -> ShowS
showsTermPrec p tm = case tm of
  Var s -> showString s
  Fn f tms -> case (f, tms) of
    ("-", [t]) -> showParen (p > 3) $ showString "-" . showsTermPrec 3 t
    ("*", [t1, t2]) -> showBinOp 2 " * " t1 t2
    ("/", [t1, t2]) -> showBinOp 2 " / " t1 t2
    ("+", [t1, t2]) -> showBinOp 1 " + " t1 t2
    ("-", [t1, t2]) -> showBinOp 1 " - " t1 t2
    _ -> showString f . showParen True (foldr (.) id (intersperse (showString ", ") (map (showsTermPrec 0) tms)))
  where
    showBinOp q = genShowBinOp showsTermPrec p q ALeft

showsFOLPrec :: Int -> FOL -> ShowS
showsFOLPrec _ (R f tms) = case tms of
  [t1, t2] | f `elem` compOps -> showsTermPrec 0 t1 . showChar ' ' . showString f . showChar ' ' . showsTermPrec 0 t2
  [] -> showsTermPrec 0 (Var f)
  _ -> showsTermPrec 0 (Fn f tms)

showsFormPrec :: Int -> Form -> ShowS
showsFormPrec r fm = case fm of
  Bottom     -> showString "false"
  Top        -> showString "true"
  Atom fol   -> showsFOLPrec 0 fol
  Not p      -> showParen (r > 4) $ showString "~ " . showsFormPrec 4 p
  And p1 p2  -> showBinOp 3 " /\\ " p1 p2
  Or p1 p2   -> showBinOp 2 " \\/ " p1 p2
  Imp p1 p2  -> showBinOp 1 " ==> " p1 p2
  Iff p1 p2  -> showBinOp 1 " <=> " p1 p2
  Forall _ _ -> showParen (r > 0) $ showString "forall" . showForall fm
  Exists _ _ -> showParen (r > 0) $ showString "exists" . showExists fm
  where
    showBinOp q = genShowBinOp showsFormPrec r q ARight
    showForall (Forall x p) = showChar ' ' . showString x . showForall p
    showForall p            = showString ". " . showsFormPrec 0 p
    showExists (Exists x p) = showChar ' ' . showString x . showExists p
    showExists p            = showString ". " . showsFormPrec 0 p
