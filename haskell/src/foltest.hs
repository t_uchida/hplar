module Main where

import           Data.List          (intercalate, intersperse)
import           System.Environment

import           FOL
import           Parser
import           Types

tuples :: Int -> Form -> [[Term]]
tuples n fm = take n $ groundtuples cntms funcs $ length fvs
  where
    sfm = skolemize $ Not $ generalize fm
    fvs = fv sfm
    (consts, funcs) = herbfuns sfm
    cntms = map (Var . fst) $ uniq consts

testGroundtuples :: IO ()
testGroundtuples = do
  (x:y:_) <- getArgs
  let n = read x
  case parseForm y of
    Left err -> print err
    Right fm -> do
      let sfm = skolemize $ Not $ generalize fm
      let fvs = fv sfm
      let (consts, funcs) = herbfuns sfm
      let cntms = map (Var . fst) $ uniq consts
      putStrLn $ "sfm: " ++ show sfm
      putStrLn $ foldl (++) "fvs: " $ intersperse ", " fvs
      putStrLn $ "consts: " ++ show cntms
      putStrLn $ "funcs: " ++ show funcs
      mapM_ (putStrLn . intercalate ", " . map show) $ take n $ groundtuples cntms funcs $ length fvs

testSkolemize :: IO ()
testSkolemize = do
  x:_ <- getArgs
  case parseForm x of
    Left err -> print err
    Right fm -> print $ skolemize fm

testGilmore :: IO ()
testGilmore = do
  (x:y:_) <- getArgs
  let n = read x
  print $ flip gilmore n <$> parseForm y

testDavisPutnum :: IO ()
testDavisPutnum = do
  (x:y:_) <- getArgs
  let n = read x
  print $ flip davisputnum n <$> parseForm y

testUnify :: IO ()
testUnify = do
  args <- getArgs
  case toEqs args [] of
    Left err -> print err
    Right eqs -> do
      print eqs
      case fullunify eqs of
        Nothing -> putStrLn "Unable to unify"
        Just i  -> print i
  where
    toEqs :: [String] -> [(Term, Term)] -> Either String [(Term, Term)]
    toEqs (k:v:ts) env = ((,) <$> parseTerm k <*> parseTerm v) >>= toEqs ts . (: env)
    toEqs _ env = return env

testPrawitz :: IO ()
testPrawitz = do
  (x:y:_) <- getArgs
  case parseForm y of
    Left err -> print err
    Right fm -> loop (prawitz fm) (read x) 0
  where
    loop :: [Bool] -> Int -> Int -> IO ()
    loop [] _ _         = putStrLn "End of loop"
    loop _ m n          | m <= n = putStrLn "Count exhausted"
    loop (True:_) _ n   = putStrLn $ "Found: " ++ show n
    loop (False:bs) m n = loop bs m (n + 1)

testTableau :: IO ()
testTableau = do
  (x:y:_) <- getArgs
  case parseForm y of
    Left err -> print err
    Right fm -> loop (tab fm) (read x) 0
  where
    loop :: [Bool] -> Int -> Int -> IO ()
    loop [] _ _         = putStrLn "End of loop"
    loop _ m n          | m <= n = putStrLn "Count exhausted"
    loop (True:_) _ n   = putStrLn $ "Found: " ++ show n
    loop (False:bs) m n = loop bs m (n + 1)

main :: IO ()
main = testTableau
