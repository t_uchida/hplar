open Parser
open Lexer
open Fol
open Util

module type Proofsystem =
  sig type thm
    (* (p => q) => p => q *)
    val modusponens : thm -> thm -> thm
    (* p => forall x. p *)
    val gen : string -> thm -> thm
    (* p => q => p *)
    val axiom_addimp : fol formula -> fol formula -> thm
    (* (p => q => r) => (p => q) => (p => r) *)
    val axiom_distribimp : fol formula -> fol formula -> fol formula -> thm
    (* ((p => false) => false) => p *)
    val axiom_doubleneg : fol formula -> thm
    (* (forall x. p => q) => (forall x. p) => (forall x. q) *)
    val axiom_allimp : string -> fol formula -> fol formula -> thm
    (* p => forall x. p if x \not_in fv(p) *)
    val axiom_impall : string -> fol formula -> thm
    (* exists x. x = t if x \not_in fvt(t) *)
    val axiom_existseq : string -> term -> thm
    (* t = t *)
    val axiom_eqrefl : term -> thm
    (* s1 = t1 => ... => sn = tn => f(s1, ..., sn) = f(t1, ..., tn) *)
    val axiom_funcong : string -> term list -> term list -> thm
    (* s1 = t1 => ... => sn = tn => p(s1, ..., sn) = p(t1, ..., tn) *)
    val axiom_predcong : string -> term list -> term list -> thm
    (* (p <=> q) => p => q *)
    val axiom_iffimp1 : fol formula -> fol formula -> thm
    (* (p <=> q) => q => p *)
    val axiom_iffimp2 : fol formula -> fol formula -> thm
    (* (p => q) => (q => p) => (p <=> q) *)
    val axiom_impiff : fol formula -> fol formula -> thm
    (* true <=> (false => false) *)
    val axiom_true : thm
    (* ~p <=> (p => false) *)
    val axiom_not : fol formula -> thm
    (* p /\ q <=> (p => q => false) => false *)
    val axiom_and : fol formula -> fol formula -> thm
    (* p \/ q <=> ~(~p /\ ~q) *)
    val axiom_or : fol formula -> fol formula -> thm
    (* (exists x. p) <=> ~(forall x. ~p) *)
    val axiom_exists : string -> fol formula -> thm
    val concl : thm -> fol formula
  end;;

module FmSet = Set.Make (struct type t = fol formula let compare = compare end);;

let unions_fm l = FmSet.elements (FmSet.of_list (List.concat l));;
let union_fm x y = unions_fm [x; y];;

let rec onatoms f = function
  | Atom a -> f a 
  | Not p -> Not (onatoms f p)
  | And (p, q) -> And (onatoms f p, onatoms f q)
  | Or (p, q) -> Or (onatoms f p, onatoms f q)
  | Imp (p, q) -> Imp (onatoms f p, onatoms f q)
  | Iff (p, q) -> Iff (onatoms f p, onatoms f q)
  | Forall (x, p) -> Forall (x, onatoms f p)
  | Exists (x, p) -> Exists (x, onatoms f p)
  | fm -> fm;;

let onformula f = onatoms (fun (R (p, a)) -> Atom (R (p, List.map f a)));;

let rec termsize tm =
  match tm with
    Var x -> 1
  | Fn(f,args) -> List.fold_right (fun t n -> termsize t + n) args 1;; 

let rec occurs_in s t =
  s = t || match t with Var y -> false | Fn (f, args) -> List.exists (occurs_in s) args;;

let rec free_in t = function
  | False | True -> false
  | Atom (R (p, args)) -> List.exists (occurs_in t) args
  | Not p -> free_in t p
  | And (p, q) | Or (p, q) | Imp (p, q) | Iff (p, q) -> free_in t p || free_in t q
  | Forall (y, p) | Exists (y, p) -> not (occurs_in (Var y) t) && free_in t p;;

let mk_eq t1 t2 = Atom (R ("=", [t1; t2]));;
let mk_imp p q = Imp (p, q);;

module Proven : Proofsystem =
  struct
    type thm = fol formula
    let modusponens pq p = match pq with Imp (p', q) when p = p' -> q | _ -> failwith "modusponens"
    let gen x p = Forall (x, p)
    let axiom_addimp p q = Imp (p, Imp (q, p))
    let axiom_distribimp p q r = Imp (Imp (p, Imp (q, r)), Imp (Imp (p, q), Imp (p, r)))
    let axiom_doubleneg p = Imp (Imp (Imp (p, False), False), p)
    let axiom_allimp x p q = Imp (Forall (x, Imp (p, q)), Imp (Forall (x, p), Forall (x, q)))
    let axiom_impall x p =
      if not (free_in (Var x) p)
        then Imp (p, Forall (x, p))
        else failwith "axiom_impall: variable free in formula"
    let axiom_existseq x t =
      if not (occurs_in (Var x) t)
        then Exists (x, mk_eq (Var x) t)
        else failwith "axiom_existseq: variable free in formula"
    let axiom_eqrefl t = mk_eq t t
    let axiom_funcong f lefts rights =
      let goal = mk_eq (Fn (f, lefts)) (Fn (f, rights)) in
      List.fold_right mk_imp (List.map2 mk_eq lefts rights) goal
    let axiom_predcong p lefts rights =
      let goal = Imp (Atom (R (p, lefts)), Atom (R (p, rights))) in
      List.fold_right mk_imp (List.map2 mk_eq lefts rights) goal
    let axiom_iffimp1 p q = Imp (Iff (p, q), Imp (p, q))
    let axiom_iffimp2 p q = Imp (Iff (p, q), Imp (q, p))
    let axiom_impiff p q = Imp (Imp (p, q), Imp (Imp (q, p), Iff (p, q)))
    let axiom_true = Iff (True, Iff (False, False))
    let axiom_not p = Iff (Not p, Imp (p, False))
    let axiom_and p q = Iff (And (p, q), Imp (Imp (p, Imp (q, False)), False))
    let axiom_or p q = Iff (Or (p, q), Not (And (Not p, Not q)))
    let axiom_exists x p = Iff (Exists (x, p), Not (Forall (x, Not p)))
    let concl c = c
  end;;

include Proven

let print_thm th =
  Format.open_box 0;
  print_string "|- ";
  Format.open_box 0;
  print_formula print_atom (concl th);
  Format.close_box ();
  Format.close_box ();
  Format.print_newline ();;

let dest_imp = function
  | Imp (p, q) -> (p, q)
  | fm -> (print_fol_formula fm (); failwith "dest_imp");;

let dest_iff = function
  | Iff (p, q) -> (p, q)
  | _ -> failwith "dest_iff";;

let dest_and = function
  | And (p, q) -> (p, q)
  | _ -> failwith "dest_and"

let dest_eq = function
  | Atom (R ("=", [s; t])) -> s, t
  | _ -> failwith "dest_eq";;

let consequent = function
  | Imp (p, q) -> q
  | _ -> failwith "consequent";;

let antecedent = function
  | Imp (p, q) -> p
  | _ -> failwith "consequent";;

(* p => p => p *)
let imp_refl p =
  modusponens (modusponens (axiom_distribimp p (Imp (p, p)) p) (axiom_addimp p (Imp (p, p))))
              (axiom_addimp p p);;

(* (p => p => q) => p => q *)
let imp_unduplicate th =
  let p, pq = dest_imp (concl th) in
  let q = consequent pq in
  modusponens (modusponens (axiom_distribimp p p q) th) (imp_refl p);;

let negatef = function Imp (p, False) -> p | p -> Imp (p, False);;

let negativef = function Imp (p, False) -> true | _ -> false;;

let add_assum p th = modusponens (axiom_addimp (concl th) p) th;;

(* p => (q => r) => (p => q) => p => r *)
let imp_add_assum p th = let (q, r) = dest_imp (concl th) in modusponens (axiom_distribimp p q r) (add_assum p th);;

(* (p => q) => (q => r) => p => r *)
let imp_trans th1 th2 = let p = antecedent (concl th1) in modusponens (imp_add_assum p th2) th1;;

(* (p => r) => p => q => r *)
let imp_insert q th = let (p, r) = dest_imp (concl th) in imp_trans th (axiom_addimp r q);;

(* (p => q => r) => q => p => r *)
let imp_swap th =
  let p, qr = dest_imp (concl th) in
  let q, r = dest_imp qr in
  imp_trans (axiom_addimp q p) (modusponens (axiom_distribimp p q r) th);;

(* (q => r) => (p => q) => p => r *)
let imp_trans_th p q r = imp_trans (axiom_addimp (Imp (q, r)) p) (axiom_distribimp p q r);;

(* (p => q) => (q => r) => p => r *)
let imp_add_concl r th = let (p, q) = dest_imp (concl th) in modusponens (imp_swap (imp_trans_th p q r)) th;;

(* (p => q => r) => q => p => r *)
let imp_swap_th p q r =
  imp_trans (axiom_distribimp p q r) (imp_add_concl (Imp (p, r)) (axiom_addimp q p));;

(* ((p => q => r) => s => t => u) => (q => p => r) => t => s => u *)
let imp_swap2 th =
  match concl th with
  | Imp (Imp (p, Imp (q, r)), Imp (s, Imp (t, u))) -> imp_trans (imp_swap_th q p r) (imp_trans th (imp_swap_th s t u))
  | _ -> failwith "imp_swap2";;

(* (p => q => r) => (p => q) => p => r *)
let right_mp ith th = imp_unduplicate (imp_trans th (imp_swap ith));;

(* (p <=> q) => p => q *)
let iff_imp1 th = let (p, q) = dest_iff (concl th) in modusponens (axiom_iffimp1 p q) th;;

(* (p <=> q) => q => p *)
let iff_imp2 th = let (p, q) = dest_iff (concl th) in modusponens (axiom_iffimp2 p q) th;;

(* (p => q) => (q => p) => (p <=> q) *)
let imp_antisym th1 th2 = let (p, q) = dest_imp (concl th1) in modusponens (modusponens (axiom_impiff p q) th1) th2;;

(* (p => (q => false) => false) => p => q *)
let right_doubleneg th =
  match concl th with
  | Imp (_, Imp (Imp (p, False), False)) -> imp_trans th (axiom_doubleneg p)
  | _ -> failwith "right_doubleneg";;

(* false => p *)
let ex_falso p = right_doubleneg (axiom_addimp False (Imp (p, False)));;

(* (p => q => r) => (r => s) => p => q => s *)
let imp_trans2 th1 th2 =
  match (concl th1, concl th2) with
  | Imp (p, Imp (q, r)), Imp (r', s) ->
      let th = imp_add_assum p (modusponens (imp_trans_th q r s) th2) in modusponens th th1
  | _ -> failwith "imp_trans2";;

(* (p => q1) => ... => (p => qn) => (q1 => ... => qn => r) => p => r *)
let imp_trans_chain ths th =
  List.fold_right (fun a b -> imp_unduplicate (imp_trans a (imp_swap b))) (List.rev (List.tl ths)) (imp_trans (List.hd ths) th);;

(* (q => false) => p => (p => q) => false *)
let imp_truefalse p q = imp_trans (imp_trans_th p q False) (imp_swap_th (Imp (p, q)) p False);;

(* (p => p') => (q => q') => (p => q) => p' => q' *)
let imp_mono_th p p' q q' =
  let th1 = imp_trans_th (Imp (p, q)) (Imp (p', q)) (Imp (p', q'))
  and th2 = imp_trans_th p' q q'
  and th3 = imp_swap (imp_trans_th p' p q) in
  imp_trans th3 (imp_swap (imp_trans th2 th1));;

(* true *)
(* let truth () = modusponens (iff_imp2 axiom_true) (imp_refl False);; *)
let truth = modusponens (iff_imp2 axiom_true) (imp_antisym (imp_refl False) (imp_refl False));;

(* (p => q) => ~q => ~p *)
let contrapos th =
  let p, q = dest_imp (concl th) in
  imp_trans (imp_trans (iff_imp1 (axiom_not q)) (imp_add_concl False th)) (iff_imp2 (axiom_not p));;

(* p /\ q => p *)
let and_left p q =
  let th1 = imp_add_assum p (axiom_addimp False p) in
  let th2 = right_doubleneg (imp_add_concl False th1) in
  imp_trans (iff_imp1 (axiom_and p q)) th2;;

(* p /\ q => q *)
let and_right p q =
  let th1 = axiom_addimp (Imp (q, False)) p in
  let th2 = right_doubleneg (imp_add_concl False th1) in
  imp_trans (iff_imp1 (axiom_and p q)) th2;;

(* p1 /\ ... /\ pn => pi (1 <= i <= n) *)
let rec conjths fm =
  try let p, q = dest_and fm in (and_left p q) :: List.map (imp_trans (and_right p q)) (conjths q)
  with Failure _ -> [imp_refl fm];;

(* p => q => p /\ q *)
let and_pair p q =
  let th1 = iff_imp2 (axiom_and p q) in
  let th2 = imp_swap_th (Imp (p, Imp (q, False))) q False in
  let th3 = imp_add_assum p (imp_trans2 th2 th1) in
  modusponens th3 (imp_swap (imp_refl (Imp (p, Imp (q, False)))));;

(* (p /\ q => r) => p => q => r *)
let shunt th =
  let p, q = dest_and (antecedent (concl th)) in modusponens (imp_add_assum p (imp_add_assum q th)) (and_pair p q);;

(* (p => q => r) => (p /\ q) => r *)
let unshunt th =
  let p, qr = dest_imp (concl th) in
  let q, r = dest_imp qr in
  imp_trans_chain [and_left p q; and_right p q] th;;

(* (p <=> q) <=> (p => q) /\ (q => p) *)
let iff_def p q =
  let th = and_pair (Imp (p, q)) (Imp (q, p))
  and thl = [axiom_iffimp1 p q; axiom_iffimp2 p q] in
  imp_antisym (imp_trans_chain thl th) (unshunt (axiom_impiff p q));;

let expand_connective = function
  | True -> axiom_true
  | Not p -> axiom_not p
  | And (p, q) -> axiom_and p q
  | Or (p, q) -> axiom_or p q
  | Iff (p, q) -> iff_def p q
  | Exists (x, p) -> axiom_exists x p
  | _ -> failwith "expand_connective";;

let eliminate_connective fm =
  if not (negativef fm)
    then iff_imp1 (expand_connective fm)
    else imp_add_concl False (iff_imp2 (expand_connective (negatef fm)));;

(* ((p => q) => false) => p; ((p => q) => false) => q => false *)
let imp_false_conseqs p q =
  [ right_doubleneg (imp_add_concl False (imp_add_assum p (ex_falso q)))
  ; imp_add_concl False (imp_insert p (imp_refl q))
  ] ;;

let rec funpow n f x = if n < 1 then x else funpow (n-1) f (f x);;

(* (p => (q => false) => r) => ((p => q) => false) => r *)
let imp_false_rule th =
  let p, r = dest_imp (concl th) in
  imp_trans_chain (imp_false_conseqs p (funpow 2 antecedent r)) th;;

(* ((p => false) => r) => (q => r) => (p => q) => r *)
let imp_true_rule th1 th2 =
  let p = funpow 2 antecedent (concl th1)
  and q = antecedent (concl th2)
  and th3 = right_doubleneg (imp_add_concl False th1)
  and th4 = imp_add_concl False th2 in
  let th5 = imp_swap (imp_truefalse p q) in
  let th6 = imp_add_concl False (imp_trans_chain [th3; th4] th5)
  and th7 = imp_swap (imp_refl (Imp (Imp (p, q), False))) in
  right_doubleneg (imp_trans th7 th6);;

(* p => ~p => q *)
let imp_contr p q =
  if negativef p
    then imp_add_assum (negatef p) (ex_falso q)
    else imp_swap (imp_add_assum p (ex_falso q));;

(* (p1 => ... => pn-1 => pn) => pn => p1 => ... => pn-1 *)
let rec imp_front_th n fm =
  if n = 0 then imp_refl fm
  else
    let p, qr = dest_imp fm in
    let th1 = imp_add_assum p (imp_front_th (n - 1) qr) in
    let q', r' = dest_imp (funpow 2 consequent (concl th1)) in
    imp_trans th1 (imp_swap_th p q' r');;

(* (p1 => ... => pi => ... => pn) => pi => p1 => ... => pn *)
let imp_front n th = modusponens (imp_front_th n (concl th)) th;;

let rec chop_list n l =
  if n = 0 then [],l else
  try let m,l' = chop_list (n-1) (List.tl l) in (List.hd l)::m,l'
  with Failure _ -> failwith "chop_list";;

let index x =
  let rec go n = function
    | x' :: l -> if x = x' then n else go (succ n) l
    | [] -> failwith "index" in
  go 0;;

let rec lcfptab fms lits =
  match fms with
  | False :: fl -> ex_falso (List.fold_right mk_imp (fl @ lits) False)
  | (Imp (p, q) as fm) :: fl when p = q -> add_assum fm (lcfptab fl lits)
  | Imp (Imp (p, q), False) :: fl -> imp_false_rule (lcfptab (p :: Imp (q, False) :: fl) lits)
  | Imp (p, q) :: fl when q <> False -> imp_true_rule (lcfptab (Imp (p, False) :: fl) lits) (lcfptab (q :: fl) lits)
  | (Atom (_) | Forall (_, _) | Imp ((Atom (_) | Forall (_, _)), False) as p) :: fl ->
      if List.mem (negatef p) lits then
        let l1, l2 = chop_list (index (negatef p) lits) lits in
        let th = imp_contr p (List.fold_right mk_imp (List.tl l2) False) in
        List.fold_right imp_insert (fl @ l1) th
      else imp_front (List.length fl) (lcfptab fl (p :: lits))
  | fm :: fl -> let th = eliminate_connective fm in imp_trans th (lcfptab (consequent (concl th) :: fl) lits)
  | _ -> failwith "lcfptab: no contradiction"

let lcftaut p = modusponens (axiom_doubleneg p) (lcfptab [negatef p] []);;

let eq_sym s t =
  let rth = axiom_eqrefl s in
  funpow 2 (fun th -> modusponens (imp_swap th) rth) (axiom_predcong "=" [s; s] [t; s])

let eq_trans s t u =
  let th1 = axiom_predcong "=" [t; u] [s; u] in
  let th2 = modusponens (imp_swap th1) (axiom_eqrefl u) in
  imp_trans (eq_sym s t) th2;;

let lhs = function Atom (R ("=", [t; _])) -> t | _ -> failwith "lhs";;

let rhs = function Atom (R ("=", [_; t])) -> t | _ -> failwith "rhs";;

let rec icongruence s t stm ttm =
  if stm = ttm then add_assum (mk_eq s t) (axiom_eqrefl stm)
  else if stm = s && ttm = t then imp_refl (mk_eq s t) else
  match (stm, ttm) with
  | (Fn (fs, sa), Fn (ft, ta)) when fs = ft && List.length sa = List.length ta ->
      let ths = List.map2 (icongruence s t) sa ta in
      let ts = List.map (fun x -> consequent (concl x)) ths in
      imp_trans_chain ths (axiom_funcong fs (List.map lhs ts) (List.map rhs ts))
  | _ -> failwith "icongruence: not congruent";;

(* (forall x. p => Q[x]) => p => (forall x. Q[x]) *)
let gen_right_th x p q = imp_swap (imp_trans (axiom_impall x p) (imp_swap (axiom_allimp x p q)));;

(* (P[x] => Q[x]) => (forall x. P[x]) => (forall x. Q[x]) *)
let genimp x th = let p, q = dest_imp (concl th) in modusponens (axiom_allimp x p q) (gen x th);;

(* (p => Q[x]) => p => (forall x. Q[x]) *)
let gen_right x th = let p, q = dest_imp (concl th) in modusponens (gen_right_th x p q) (gen x th);;

(* (forall x. P[x] => q) => (exists x. P[x]) => q *)
let exists_left_th x p q =
  let p' = Imp (p, False) in
  let q' = Imp (q, False) in
  let th1 = genimp x (imp_swap (imp_trans_th p q False)) in
  let th2 = imp_trans th1 (gen_right_th x q' p') in
  let th3 = imp_swap (imp_trans_th q' (Forall (x, p')) False) in
  let th4 = imp_trans2 (imp_trans th2 th3) (axiom_doubleneg q) in
  let th5 = imp_add_concl False (genimp x (iff_imp2 (axiom_not p))) in
  let th6 = imp_trans (iff_imp1 (axiom_not (Forall (x, Not p)))) th5 in
  let th7 = imp_trans (iff_imp1 (axiom_exists x p)) th6 in
  imp_swap (imp_trans th7 (imp_swap th4));;

(* (P[x] => q) => (exists x. P[x]) => q (if x \not_in FV(q)) *)
let exists_left x th = let p, q = dest_imp (concl th) in modusponens (exists_left_th x p q) (gen x th);;

(* (x = t => P[x] => P[t] (x \not_in FVT(t))) => (forall x. P[x]) => P[t] *)
let subspec th =
  match concl th with
  | Imp (Atom (R ("=", [Var x; t])) as e, Imp (p, q)) ->
      let th1 = imp_trans (genimp x (imp_swap th)) (exists_left_th x e q) in
      modusponens (imp_swap th1) (axiom_existseq x t)
  | _ -> failwith "subspec: wrong sort of theorem";;

(* (x = y => P[x] => Q[y]) => (forall x. P[x]) => forall y. Q[y] *)
let subalpha th =
  match concl th with
  | Imp (Atom (R ("=", [Var x; Var y])), Imp (p, q)) ->
      if x = y then genimp x (modusponens th (axiom_eqrefl (Var x)))
      else gen_right y (subspec th)
  | _ -> failwith "subalpha: wrong sort of theorem"

let unzip ls = List.map fst ls, List.map snd ls;;

let rec variant x = function
  | [] -> x
  | y :: ys -> let x' = if x = y then (x ^ "'") else x in variant x' ys;;

let rec isubst s t sfm tfm =
  if sfm = tfm then add_assum (mk_eq s t) (imp_refl tfm) else
  match (sfm, tfm) with
  | Atom (R (p, sa)), Atom (R (p', ta)) when p = p' && List.length sa = List.length ta ->
      let ths = List.map2 (icongruence s t) sa ta in
      let ls, rs = unzip (List.map (fun x -> dest_eq (consequent (concl x))) ths) in
      imp_trans_chain ths (axiom_predcong p ls rs)
  | Imp (sp, sq), Imp (tp, tq) ->
      let th1 = imp_trans (eq_sym s t) (isubst t s tp sp) in
      let th2 = isubst s t sq tq in
      imp_trans_chain [th1; th2] (imp_mono_th sp tp sq tq)
  | Forall (x, p), Forall (y, q) ->
      if x = y then imp_trans (gen_right x (isubst s t p q)) (axiom_allimp x p q)
      else
        let z = Var (variant x (unions [fv p; fv q; fvt s; fvt t])) in
        let th1 = isubst (Var x) z p (subst (valmod' x z) p)
        and th2 = isubst z (Var y) (subst (valmod' y z) p) q in
        let th3 = subalpha th1
        and th4 = subalpha th2 in
        let th5 = isubst s t (consequent (concl th3)) (antecedent (concl th4)) in
        imp_swap (imp_trans2 (imp_trans th3 (imp_swap th5)) th4)
  | _ ->
      let sth = iff_imp1 (expand_connective sfm)
      and tth = iff_imp2 (expand_connective tfm) in
      let th1 = isubst s t (consequent (concl sth))
                           (antecedent (concl tth)) in
      imp_swap (imp_trans sth (imp_swap (imp_trans2 th1 tth)));;

let alpha z = function
  | Forall (x, p) -> let p' = subst (valmod' x (Var z)) p in subalpha (isubst (Var x) (Var z) p p')
  | _ -> failwith "alpha: not a universal formula";;

let rec ispec t fm =
  match fm with
  | Forall (x, p) ->
      if List.mem x (fvt t) then
        let th = alpha (variant x (union (fvt t) (var p))) fm in
        imp_trans th (ispec t (consequent (concl th)))
      else subspec (isubst (Var x) t p (subst (valmod' x t) p))
  | _ -> failwith "ispec: non-universal formula";;

let spec t th = modusponens (ispec t (concl th)) th;;

let unify_complementsf env = function
  | Atom (R (p1, a1)), Imp (Atom (R (p2, a2)), False)
  | Imp (Atom (R (p1, a1)), False), Atom (R (p2, a2))
      -> unify env [Fn (p1, a1), Fn (p2, a2)]
  | _ -> failwith "unify_complementsf";;

let rec use_laterimp i = function
  | Imp (Imp (q', s), Imp (Imp (q, p) as i', r)) when i' = i ->
     let th1 = axiom_distribimp i (Imp (Imp (q, s), r)) (Imp (Imp (p, s), r)) in
     let th2 = imp_swap (imp_trans_th q p s) in
     let th3 = imp_swap (imp_trans_th (Imp (p, s)) (Imp (q, s)) r) in
     imp_swap2 (modusponens th1 (imp_trans th2 th3))
  | Imp (qs, Imp (a, b)) ->
     imp_swap2 (imp_add_assum a (use_laterimp i (Imp (qs, b))))
  | _ -> failwith "use_laterimp: pattern mismatch";;

let imp_false_rule' th es = imp_false_rule (th es);;

let imp_true_rule' th1 th2 es = imp_true_rule (th1 es) (th2 es);;

let imp_front' n thp es = imp_front n (thp es);;

let add_assum' fm thp (e, s as es) = add_assum (onformula e fm) (thp es);;

let eliminate_connective' fm thp (e, s as es) = imp_trans (eliminate_connective (onformula e fm)) (thp es);;

let spec' y fm n thp (e, s) =
  let th = imp_swap (imp_front n (thp (e, s))) in
  imp_unduplicate (imp_trans (ispec (e y) (onformula e fm)) th);;

let ex_falso' fms (e, s) = ex_falso (List.fold_right (fun x -> mk_imp (onformula e x)) fms s);;

let complits' (fms, lits) i (e, s) =
  match fms, chop_list i lits with
  | p :: fl, (l1, p' :: l2) ->
     List.fold_right (fun x-> imp_insert (onformula e x))
                     (fl @ l1)
                     (imp_contr (onformula e p) (List.fold_right (fun x -> mk_imp (onformula e x)) l2 s))
  | _ -> failwith "complits': pattern mismatch";;

let deskol' (skh : fol formula) thp (e, s) =
  let th = thp (e, s) in
  modusponens (use_laterimp (onformula e skh) (concl th)) th;;

let rec lcftab skofun (fms, lits, n) cont (env, sks, k as esk) =
  if n < 0 then failwith "lcftab: no proof" else
    match fms with
    | False :: fl -> cont (ex_falso' (fl @ lits)) esk
    | (Imp (p, q) as fm) :: fl when p = q ->
       lcftab skofun (fl, lits, n) (fun x -> cont (add_assum' fm x)) esk
    | Imp (Imp (p, q), False) :: fl ->
       lcftab skofun (p :: Imp (q, False) :: fl, lits, n) (fun x -> cont (imp_false_rule' x)) esk
    | Imp (p, q) :: fl when q <> False ->
       lcftab skofun (Imp (p, False) :: fl, lits, n) (fun th -> lcftab skofun (q :: fl, lits, n) (fun x -> cont (imp_true_rule' th x))) esk
    | ((Atom _ | Imp (Atom _, False)) as p) :: fl ->
       (try tryfind (fun p' ->
                let env' = unify_complementsf env (p, p') in
                cont (complits' (fms, lits) (index p' lits)) (env', sks, k)) lits
        with Failure _ ->
          lcftab skofun (fl, p :: lits, n) (fun x -> cont (imp_front' (List.length fl) x)) esk)
    | (Forall (x, p) as fm) :: fl ->
       let y = Var ("X_" ^ string_of_int k) in
       lcftab skofun ((subst (valmod' x y) p) :: fl @ [fm], lits, n - 1) (fun x -> cont (spec' y fm (List.length fms) x)) (env, sks, k + 1)
    | (Imp (Forall (y, p) as yp, False)) :: fl ->
       let fx = skofun yp in
       let p' = subst (valmod' y fx) p in
       let skh = Imp (p', Forall (y, p)) in
       let sks' = (Forall (y, p), fx) :: sks in
       lcftab skofun (Imp (p', False) :: fl, lits, n) (fun x -> cont (deskol' skh x)) (env, sks', k)
    | fm :: fl ->
       let fm' = consequent (concl (eliminate_connective fm)) in
       lcftab skofun (fm' :: fl, lits, n) (fun x -> cont (eliminate_connective' fm x)) esk
    | [] -> failwith "lcftab: No contradiction";;

let rec quantforms e fm =
  match fm with
  | Not p -> quantforms (not e) p
  | And (p, q) | Or (p, q) -> union_fm (quantforms e p) (quantforms e q)
  | Imp (p, q) -> quantforms e (Or (Not p, q))
  | Iff (p, q) -> quantforms e (Or (And (p, q), And (Not p, Not q)))
  | Exists (x, p) -> if e then (fm :: quantforms e p) else quantforms e p
  | Forall (x, p) -> if e then quantforms e p else (fm :: quantforms e p)
  | _ -> [];;

let skolemfuns fm =
  let fns = List.map fst (functions fm) in
  let skts = List.map (function Exists (x, p) -> Forall (x, Not p) | p -> p) (quantforms true fm) in
  let skofun i = function
    | Forall (y, p) as ap ->
       let vars = List.map (fun v -> Var v) (fv ap) in
       ap, Fn (variant ("f_" ^ string_of_int i) fns, vars)
    | _ -> failwith "skolemfuns: pattern mismatch" in
  List.map2 skofun (1 -- List.length skts) skts;;

let rec term_match (t1, t2 as tp) =
  match tp with
  | Var x1, Var x2 -> x1 = x2
  | Fn (f1, ts1), Fn (f2, ts2) -> f1 = f2 && List.for_all term_match (List.combine ts1 ts2)
  | _ -> false;;

let rec form_match (f1, f2 as fp) env =
  match fp with
  | False, False | True, True -> env
  | Atom (R (p, pa)), Atom (R (q, qa)) ->
     if term_match (Fn (p, pa), Fn (q, qa)) then env else failwith "form_match: unmatched"
  | Not p1, Not p2 -> form_match (p1, p2) env
  | And (p1, q1), And (p2, q2) | Or (p1, q1), Or (p2, q2)
  | Imp (p1, q1), Imp (p2, q2) | Iff (p1, q1), Iff (p2, q2) -> form_match (p1, p2) (form_match (q1, q2) env)
  | Forall (x1, p1), Forall (x2, p2) | Exists (x1, p1), Exists (x2, p2) when x1 = x2 ->
     let z = variant x1 (union (fv p1) (fv p2)) in
     let inst_fn = subst (valmod' x1 (Var z)) in
     undefine z (form_match (inst_fn p1, inst_fn p2) env)
  | _ -> failwith "form_match: unmatched";;

let lcfrefute fm n cont =
  let sl = skolemfuns fm in
  let find_skolem fm = tryfind (fun (f, t) -> tsubst (form_match (f, fm) undefined) t) sl in
  lcftab find_skolem ([fm], [], n) cont (undefined, [], 0);;

let mk_skol th q =
  match th with
  | Forall (y, p), fx -> Imp (Imp (subst (valmod' y fx) p, Forall (y, p)), q)
  | _ -> failwith "mk_skol: pattern mismatch";;

let simpcont thp (env, sks, k) =
  let ifn = tsubst (solve env) in
  thp (ifn, onformula ifn (List.fold_right mk_skol sks False));;

let elim_skolemvar th =
  match concl th with
  | Imp (Imp (pv, (Forall (x, px) as apx)), q) ->
     let ths = List.map (imp_trans (imp_add_concl False th)) (imp_false_conseqs pv apx) in
     (match ths with
      | [th1; th2] ->
          let v = List.hd (subtract (fv pv) (fv apx) @ [x]) in
          let th3 = gen_right v th1 in
          let th4 = imp_trans th3 (alpha x (consequent (concl th3))) in
          modusponens (axiom_doubleneg q) (right_mp th2 th4)
     | _ -> failwith "elim_skolemvar")
  | _ -> failwith "elim_skolemvar";;

let rec replacet rfn tm =
  try rfn tm with Failure _ ->
  match tm with
    Fn(f,args) -> Fn(f, List.map (replacet rfn) args)
  | _ -> tm;;

let replace rfn = onformula (replacet rfn);;

let deskolcont thp (env, sks, k) =
  let module S = Set.Make (struct type t = fol formula * term let compare = compare end) in
  let uni s = S.elements (S.of_list s) in
  let ifn = tsubst (solve env) in
  let isk = uni (List.map (fun (p, t) -> onformula ifn p, ifn t) sks) in
  let ssk = List.sort (decreasing (fun x -> termsize (snd x))) isk in
  let vs = List.map (fun i -> Var ("Y_" ^ string_of_int i)) (1 -- List.length ssk) in
  let vfn = replacet (itlist2 (fun (_, t) v -> valmod t v) ssk vs undefined) in
  let th = thp ((fun x -> vfn (ifn x)), onformula vfn (List.fold_right mk_skol ssk False)) in
  repeat (fun x -> elim_skolemvar (imp_swap x)) th;;

let rec deepen f n =
  try print_string "Searching with depth limit ";
      print_int n;
      print_newline ();
      f n
  with Failure _ -> deepen f (n + 1);;

let lcffol fm =
  let fvs = fv fm in
  let fm' = Imp (List.fold_right mk_forall fvs fm, False) in
  let th1 = deepen (fun n -> lcfrefute fm' n deskolcont) 0 in
  let th2 = modusponens (axiom_doubleneg (negatef fm')) th1 in
  List.fold_right (fun v -> spec (Var v)) (List.rev fvs) th2;;

type goals = Goals of ((string * fol formula) list * fol formula) list * (thm list -> thm);;

let print_goal (Goals (gls, jfn)) =
  let print_hyp (l, fm) =
    Format.open_hbox ();
    print_string (l ^ ":");
    Format.print_space ();
    print_formula print_atom fm;
    print_newline();
    Format.close_box () in
  match gls with
  | (asl, w) :: ogls ->
     print_newline ();
     (if ogls = [] then print_string "1 subgoal:" else
        (print_int (List.length gls);
         print_string " subgoals starting with"));
     print_newline ();
     List.iter print_hyp (List.rev asl);
     print_string "---> ";
     Format.open_hvbox 0;
     print_formula print_atom w;
     Format.close_box ();
     print_newline ()
  | [] -> print_string "No subgoals";;

let set_goal p =
  let chk th = if concl th = p then th else failwith "set_goal: wrong theorem" in
  Goals ([[], p], function [th] -> chk (modusponens th truth) | _ -> failwith "set_goal: pattern mismatch");;

let extract_thm = function
  | Goals ([], jfn) -> jfn []
  | _ -> failwith "extract_thm: unsolved goals";;

let tac_proof g prf = extract_thm (List.fold_right (fun f -> f) (List.rev prf) g);;

let prove p prf = tac_proof (set_goal p) prf;;

let conj_intro_tac = function
  | (Goals ((asl, And (p, q)) :: gls, jfn)) ->
    let jfn' = function (thp :: thq :: ths) -> jfn (imp_trans_chain [thp; thq] (and_pair p q) :: ths) | _ -> failwith "conj_intro_tac: pattern mismatch" in
    Goals ((asl, p) :: (asl, q) :: gls, jfn')
  | _ -> failwith "conj_intro_tac: pattern mismatch";;

let jmodify jfn tfn = function
  | th :: oths -> jfn (tfn th :: oths)
  | _ -> failwith "jmodify: pattern mismatch";;

let gen_right_alpha y x th =
  let th1 = gen_right y th in
  imp_trans th1 (alpha x (consequent (concl th1)));;

let forall_intro_tac y = function
  | (Goals ((asl, (Forall (x, p) as fm)) :: gls, jfn)) ->
      if List.mem y (fv fm) || List.exists (fun aa -> List.mem y (fv (snd aa))) asl
      then failwith "fix: variable already free in goal" else
        Goals ((asl, subst (valmod' x (Var y)) p) :: gls, jmodify jfn (gen_right_alpha y x))
  | _ -> failwith "forall_intro_tac: pattern mismatch"

let right_exists x t p =
  let th = contrapos (ispec t (Forall (x, Not p))) in
  match antecedent (concl th) with
  | Not (Not p') ->
     List.fold_right imp_trans [imp_contr p' False; imp_add_concl False (iff_imp1 (axiom_not p')); iff_imp2 (axiom_not (Not p')); th] (iff_imp2 (axiom_exists x p))
  | _ -> failwith "right_exists: pattern mismatch";;

let exists_intro_tac t = function
  | Goals ((asl, Exists (x, p)) :: gls, jfn) ->
      Goals ((asl, subst (valmod' x t) p) :: gls, jmodify jfn (fun th -> imp_trans th (right_exists x t p)))
  | _ -> failwith "exists_intro_tac: pattern mismatch";;

let imp_intro_tac s = function
  | Goals ((asl, Imp (p, q)) :: gls, jfn) ->
     let jmod = if asl = [] then add_assum True else fun ts -> imp_swap (shunt ts) in
     Goals (((s, p) :: asl, q) :: gls, jmodify jfn jmod)
  | _ -> failwith "imp_intro_tac: pattern mismatch";;

let rec list_conj = function
  | [] -> True
  | [x; y] -> mk_and x y
  | x :: xs -> mk_and x (list_conj xs);;

let rec list_disj = function
  | [] -> False
  | [x; y] -> mk_or x y
  | x :: xs -> mk_or x (list_disj xs);;

let assumptate g th =
  match g with
  | Goals ((asl, w) :: gls, jfn) -> add_assum (list_conj (List.map snd asl)) th
  | _ -> failwith "assumptate: pattern mismatch";;

let using ths p g =
  let ths' = List.map (fun th -> List.fold_right gen (fv (concl th)) th) ths in
  List.map (assumptate g) ths';;

let rec assumps = function
  | [] -> []
  | [l, p] -> [l, imp_refl p]
  | (l, p) :: lps ->
     let ths = assumps lps in
     let q = antecedent (concl (snd (List.hd ths))) in
     let rth = and_right p q in
     (l, and_left p q) :: List.map (fun (l, th) -> l, imp_trans rth th) ths;;

let firstassum asl =
  let p = snd (List.hd asl) in
  let q = list_conj (List.map snd (List.tl asl)) in
  if List.tl asl = [] then imp_refl p else and_left p q;;

let by hyps = function
  | Goals ((asl, w) :: gls, jfn) -> let ths = assumps asl in List.map (fun s -> List.assoc s ths) hyps
  | _ -> failwith "by: pattern mismatch";;

let justify byfn hyps p g =
  match byfn hyps p g with
  | [th] when consequent (concl th) = p -> th
  | ths ->
     let th = lcffol (List.fold_right (fun p -> mk_imp (consequent (concl p))) ths p) in
     if ths = [] then assumptate g th else imp_trans_chain ths th;;

let proof tacs p = function
  | Goals ((asl, w) :: gls, jfn) ->
     let jfn' = function [th] -> th | _ -> failwith "proof: pattern mismatch" in
     [tac_proof (Goals ([asl, p], jfn')) tacs]
  | _ -> failwith "proof: pattern mismatch";;

let at once p gl = [] and once = [];;

let auto_tac byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as g ->
     let th = justify byfn hyps w g in
     Goals (gls, fun ths -> jfn (th :: ths))
  | _ -> failwith "auto_tac: pattern mismatch";;

let lemma_tac s p byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as g ->
     let tr = imp_trans (justify byfn hyps p g) in
     let mfn = if asl = [] then tr else (fun x -> imp_unduplicate (tr (shunt x))) in
     Goals (((s, p) :: asl, w) :: gls, jmodify jfn mfn)
  | _ -> failwith "lemma_tac: pattern mismatch";;

let exists_elim_tac l fm byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as g ->
     (match fm with
      | Exists (x, p) ->
         if List.exists (fun z -> List.mem x (fv z)) (w :: List.map snd asl)
         then failwith "exists_elim_tac: variable free in assumptions" else
           let th = justify byfn hyps (Exists (x, p)) g in
           let jfn' pth = imp_unduplicate (imp_trans th (exists_left x (shunt pth))) in
           Goals (((l, p) :: asl, w) :: gls, jmodify jfn jfn')
     | _ -> failwith "exists_elim_tac: pattern mismatch")
  | _ -> failwith "exists_elim_tac: pattern mismatch";;

let ante_disj th1 th2 =
  let p, r = dest_imp (concl th1) in
  let q, s = dest_imp (concl th2) in
  let ths = List.map contrapos [th1; th2] in
  let th3 = imp_trans_chain ths (and_pair (Not p) (Not q)) in
  let th4 = contrapos (imp_trans (iff_imp2 (axiom_not r)) th3) in
  let th5 = imp_trans (iff_imp1 (axiom_or p q)) th4 in
  right_doubleneg (imp_trans th5 (iff_imp1 (axiom_not (Imp (r, False)))));;

let disj_elim_tac l fm byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as g ->
     (match fm with
      | Or (p, q) ->
         let th = justify byfn hyps fm g in
         let jfn' = (function
           | pth :: qth :: ths ->
              let th1 = imp_trans th (ante_disj (shunt pth) (shunt qth)) in
              jfn (imp_unduplicate th1 :: ths)
           | _ -> failwith "disj_elim_tac: pattern mismatch") in
         Goals (((l, p) :: asl, w) :: ((l, q) :: asl, w) :: gls, jfn')
      | _ -> failwith "disj_elim_tac: pattern mismatch")
  | _ -> failwith "disj_elim_tac: pattern mismatch";;

let multishunt i th =
  let th1 = imp_swap (funpow i (fun x -> imp_swap (shunt x)) th) in
  imp_swap (funpow (i - 1) (fun x -> unshunt (imp_front 2 x)) th1);;

let assume lps = function
  | Goals ((asl, Imp (p, q)) :: gls, jfn) ->
     if fold_right1 mk_and (List.map snd lps) <> p then failwith "assume" else
       let jfn' th = if asl = [] then add_assum True th else multishunt (List.length lps) th in
       Goals ((lps @ asl, q) :: gls, jmodify jfn jfn')
  | _ -> failwith "assume: pattern mismatch";;

let note (l, p) = lemma_tac l p;;

let have p = note ("", p);;

let so tac arg byfn =
  tac arg (fun hyps p ->
        (function | Goals ((asl, w) :: _, _) as g1 -> firstassum asl :: byfn hyps p g1
                  | _ -> failwith "so: pattern mismatch"));;

let fix = forall_intro_tac;;

let consider (x, p) = exists_elim_tac "" (Exists (x, p));;

let take = exists_intro_tac;;

let cases fm = disj_elim_tac "" fm;;

let conclude p byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as gl ->
      let th = justify byfn hyps p gl in
      if p = w then Goals ((asl, True) :: gls, jmodify jfn (fun _ -> th)) else
        let p', q = dest_and w in
        if p' <> p then failwith "conclude: bad conclusion" else
          let mfn th' = imp_trans_chain [th; th'] (and_pair p q) in
          Goals ((asl, q) :: gls, jmodify jfn mfn)
  | _ -> failwith "conclude: pattern mismatch";;

let our thesis byfn hyps = function
  | Goals ((asl, w) :: gls, jfn) as gl -> conclude w byfn hyps gl
  | _ -> failwith "our: pattern mismatch";;

let qed = function
  | Goals ((asl, w) :: gls, jfn) as gl ->
     if w = True then Goals (gls, fun ths -> jfn (assumptate gl truth :: ths))
     else failwith "qed: non-trivial goal"
  | _ -> failwith "qed: pattern mismatch";;
