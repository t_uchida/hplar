open Parser
open Fol

let is_eq = function Atom (R ("=", _)) -> true | _ -> false;;

let mk_eq s t = Atom (R ("=", [s; t]));;

let dest_eq = function
  | Atom (R ("=", [s; t])) -> s, t
  | _ -> failwith "dest_eq: not an equation";;

let lhs eq = fst (dest_eq eq);;
let rhs eq = snd (dest_eq eq);;

let rec predicates = function
  | Atom (R (p, a)) -> [p, List.length a]
  | Not p -> predicates p
  | And (p, q) -> string_int_unions [predicates p; predicates q]
  | Or (p, q) -> string_int_unions [predicates p; predicates q]
  | Imp (p, q) -> string_int_unions [predicates p; predicates q]
  | Iff (p, q) -> string_int_unions [predicates p; predicates q]
  | Forall (x, p) -> predicates p
  | Exists (x, p) -> predicates p
  | tm -> [];;

let rec fold_right1 f = function
  | [] -> failwith "fold_right1: empty list"
  | [x] -> x
  | x :: xs -> f x (fold_right1 f xs);;

(* forall x1 ... xn y1 ... yn. x1 = y1 /\ ... /\ xn = yn -> f(x1, ..., xn) = f(y1, ..., yn) *)
let function_congruence (f, n) =
  if n = 0 then [] else
  let argnames_x = List.map (fun n -> "x" ^ (string_of_int n)) (1 -- n) in
  let argnames_y = List.map (fun n -> "y" ^ (string_of_int n)) (1 -- n) in
  let args_x = List.map (fun x -> Var x) argnames_x in
  let args_y = List.map (fun x -> Var x) argnames_y in
  let ant = fold_right1 mk_and (List.map2 mk_eq args_x args_y) in
  let con = mk_eq (Fn (f, args_x)) (Fn (f, args_y)) in
  [List.fold_right mk_forall (argnames_x @ argnames_y) (Imp (ant, con))];;

(* forall x1 ... xn y1 ... yn. x1 = y1 /\ ... /\ xn = yn -> P(x1, ..., xn) <=> P(y1, ..., yn) *)
let predicate_congruence (p, n) =
  if n = 0 then [] else
  let argnames_x = List.map (fun n -> "x" ^ (string_of_int n)) (1 -- n) in
  let argnames_y = List.map (fun n -> "y" ^ (string_of_int n)) (1 -- n) in
  let args_x = List.map (fun x -> Var x) argnames_x in
  let args_y = List.map (fun x -> Var x) argnames_y in
  let ant = fold_right1 mk_and (List.map2 mk_eq args_x args_y) in
  let con = Imp (Atom (R (p, args_x)), Atom (R (p, args_y))) in
  [List.fold_right mk_forall (argnames_x @ argnames_y) (Imp (ant, con))];;

let equivalence_axioms =
  let p s = fst (Parser.parse (Lexer.lex s)) in
  List.map p ["forall x. x = x"; "forall x y z. x = y /\\ y = z ==> y = z"];;

let equalitize fm =
  let allpreds = predicates fm in
  if not (List.mem ("=", 2) allpreds) then fm else
    let preds = List.filter (fun x -> x <> ("=", 2)) allpreds in
    let funcs = functions fm in
    let axioms = List.concat ([equivalence_axioms] @ List.map predicate_congruence preds @ List.map function_congruence funcs) in
    Imp (fold_right1 mk_and axioms, fm);;
