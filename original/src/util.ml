let explode s =
  let rec expl i l =
    if i < 0 then l else
    expl (i - 1) (s.[i] :: l) in
  expl (String.length s - 1) [];;

let matches s = let chars = explode s in fun c -> List.mem c chars;;

let space = matches " \t\n\r"
and panctuation = matches "()[]{}"
and symbolic = matches "~`!@#$%^&*-+=|\\:;<>.?/"
and numeric = matches "0123456789"
and alphanumeric = matches "abcdefghijklmnopqrstuvwxyz_'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";;

let rec fold_right1 f = function
  | [] -> failwith "fold_right1: pattern mismatch"
  | [x] -> x
  | x :: xs -> f x (fold_right1 f xs);;

let non p x = not (p x);;

let allpairs f xs ys = List.concat (List.map (fun x -> List.map (fun y -> f x y) ys) xs);;

let can f x = try f x; true with Failure _ -> false;;

let rec allsubsets = function
  | [] -> []
  | x :: xs ->
     let sets = allsubsets xs in
     sets @ List.map (fun s -> x :: s) sets;;

let allnonemptysubsets xs = List.filter (fun x -> x <> []) (allsubsets xs);;

let rec mapfilter f l =
  match l with
    [] -> []
  | (h::t) -> let rest = mapfilter f t in
              try (f h)::rest with Failure _ -> rest;;

let decreasing f x y = -1 * compare (f x) (f y);;

let rec itlist2 f l1 l2 b = 
  match (l1,l2) with
  | (h1::t1,h2::t2) -> f h1 h2 (itlist2 f t1 t2 b)
  | _ -> b

let rec repeat f x = try repeat f (f x) with Failure _ -> x;;
