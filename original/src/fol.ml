open Parser
open Lexer
open Util
open Prop

module StringSet = Set.Make(String);;
module StringIntSet = Set.Make (struct type t = string * int let compare = compare end);;

let rec termval (domain, func, pred as m) v tm =
  match tm with
  | Var x -> v x
  | Fn (f, args) -> func f (List.map (termval m v) args);;

let undefined x = raise (Match_failure ("undefined", 0, 0));;
let undefine a f x = if x = a then undefined x else f x;;
let defined f x = try (f x; true) with Match_failure _ -> false

(* valmod a y f = (a |-> y) f *)
let valmod a y f x = if x = a then y else f x;;
let valmod' a y x = if x = a then y else undefined x;;

let rec holds (domain, func, pred as m) v = function
  | False -> false
  | True -> true
  | Atom R(r, args) -> pred r (List.map (termval m v) args)
  | Not p -> not (holds m v p)
  | And (p, q) -> holds m v p && holds m v q
  | Or (p, q) -> holds m v p || holds m v q
  | Imp (p, q) -> not (holds m v p) || (holds m v q)
  | Iff (p, q) -> holds m v p = holds m v q
  | Forall (x, p) -> List.for_all (fun a -> holds m (valmod x a v) p) domain
  | Exists (x, p) -> List.exists (fun a -> holds m (valmod x a v) p) domain;;

let bool_interp =
  let func f args =
    match (f, args) with
    | ("0", []) -> false
    | ("1", []) -> true
    | ("+", [x; y]) -> not (x = y)
    | ("*", [x; y]) -> x && y
    | _ -> failwith "uninterpreted function"
  and pred p args =
    match (p, args) with
    | ("=", [x; y]) -> x = y
    | _ -> failwith "uninterpreted predicate" in
  ([false; true], func, pred);;

let range i j =
  let rec aux n acc =
    if n < i then acc else aux (n - 1) (n :: acc)
  in aux j [];;

let (--) i j = range i j;;

let mod_interp n =
  let func f args =
    match (f, args) with
    | ("0", []) -> 0
    | ("1", []) -> 1 mod n
    | ("+", [x; y]) -> (x + y) mod n
    | ("*", [x; y]) -> (x * y) mod n
    | _ -> failwith "uninterpreted function"
  and pred p args =
    match (p, args) with
    | ("=", [x; y]) -> x = y
    | _ -> failwith "uninterpreted predicate" in
  (0 -- (n - 1), func, pred);;

let unions l = StringSet.elements (StringSet.of_list (List.concat l));;
let union x y = unions [x; y];;
let subtract x y = StringSet.elements (StringSet.diff (StringSet.of_list x) (StringSet.of_list y));;

let rec fvt = function
  | Var x -> [x]
  | Fn (f, args) -> unions (List.map fvt args);;

let rec var = function
  | False | True -> []
  | Atom (R (p, args)) -> unions (List.map fvt args)
  | Not p -> var p
  | And (p, q) | Or (p, q) | Imp (p, q) | Iff (p, q) -> List.append (var p) (var q)
  | Forall (x, p) | Exists (x, p) -> x :: (var p);;

let rec fv = function
  | False | True -> []
  | Atom (R (p, args)) -> unions (List.map fvt args)
  | Not p -> fv p
  | And (p, q) | Or (p, q) | Imp (p, q) | Iff (p, q) -> union (fv p) (fv q)
  | Forall (x, p) | Exists (x, p) -> subtract (fv p) [x];;

let mk_forall x p = Forall (x, p);;
let mk_exists x p = Exists (x, p);;
let mk_and p q = And (p, q);;
let mk_or p q = Or (p, q);;

let generalize fm = List.fold_right mk_forall (fv fm) fm;;

let rec tsubst sfn tm =
  match tm with
  | Var x -> tryapplyd sfn x tm
  | Fn (f, args) -> Fn (f, List.map (tsubst sfn) args);;

let rec variant x vars =
  if List.mem x vars then variant (x ^ "'") vars else x;;

let rec subst subfn = function
  | False -> False
  | True -> True
  | Atom (R (p, args)) -> Atom (R (p, List.map (tsubst subfn) args))
  | Not p -> Not (subst subfn p)
  | And (p, q) -> And (subst subfn p, subst subfn q)
  | Or (p, q) -> Or (subst subfn p, subst subfn q)
  | Imp (p, q) -> Imp (subst subfn p, subst subfn q)
  | Iff (p, q) -> Iff (subst subfn p, subst subfn q)
  | Forall (x, p) -> substq subfn mk_forall x p
  | Exists (x, p) -> substq subfn mk_exists x p
and substq subfn quant x p =
  let x' = if List.exists (fun y -> List.mem x (fvt (tryapplyd subfn y (Var y)))) (subtract (fv p) [x])
           then variant x (fv (subst (undefine x subfn) p)) else x in
  quant x' (subst (valmod x (Var x') subfn) p);;

let psimplify1 = function
  | Not False -> True
  | Not True -> False
  | Not (Not p) -> p
  | And (_, False) | And (False, _) -> False
  | And (p, True) | And (True, p) -> p
  | Or (p, False) | Or (False, p) -> p
  | Imp (False, _) | Imp (_, True) -> True
  | Imp (True, p) -> p
  | Imp (p, False) -> Not p
  | Iff (p, True) | Iff (True, p) -> p
  | Iff (p, False) | Iff (False, p) -> Not p
  | fm -> fm;;

let rec psimplify = function
  | Not p -> psimplify1 (Not (psimplify p))
  | And (p, q) -> psimplify1 (And (psimplify p, psimplify q))
  | Or (p, q) -> psimplify1 (Or (psimplify p, psimplify q))
  | Imp (p, q) -> psimplify1 (Imp (psimplify p, psimplify q))
  | Iff (p, q) -> psimplify1 (Iff (psimplify p, psimplify q))
  | fm -> fm;;

let simplify1 fm =
  match fm with
  | Forall (x, p) | Exists (x, p) -> if List.mem x (fv p) then fm else p
  | _ -> psimplify1 fm;;

let rec simplify = function
  | Not p -> simplify1 (Not (simplify p))
  | And (p, q) -> simplify1 (And (simplify p, simplify q))
  | Or (p, q) -> simplify1 (Or (simplify p, simplify q))
  | Imp (p, q) -> simplify1 (Imp (simplify p, simplify q))
  | Iff (p, q) -> simplify1 (Iff (simplify p, simplify q))
  | Forall (x, p) -> simplify1 (Forall (x, simplify p))
  | Exists (x, p) -> simplify1 (Exists (x, simplify p))
  | fm -> fm;;

let rec nnf = function
  | And (p, q) -> And (nnf p, nnf q)
  | Or (p, q) -> Or (nnf p, nnf q)
  | Imp (p, q) -> Or (nnf (Not p), nnf q)
  | Iff (p, q) -> Or (And (nnf p, nnf q), And (nnf (Not p), nnf (Not q)))
  | Not (Not p) -> nnf p
  | Not (And (p, q)) -> Or (nnf (Not p), nnf (Not q))
  | Not (Or (p, q)) -> And (nnf (Not p), nnf (Not q))
  | Not (Imp (p, q)) -> And (nnf p, nnf (Not q))
  | Not (Iff (p, q)) -> Or (And (nnf p, nnf (Not q)), And (nnf (Not p), nnf q))
  | Forall (x, p) -> Forall (x, nnf p)
  | Exists (x, p) -> Exists (x, nnf p)
  | Not (Forall (x, p)) -> Exists (x, nnf (Not p))
  | Not (Exists (x, p)) -> Forall (x, nnf (Not p))
  | fm -> fm;;

let rec pullquants fm =
  match fm with
  | And (Forall (x, p), Forall (y, q)) -> pullq (true, true) fm mk_forall mk_and x y p q
  | Or (Exists (x, p), Exists (y, q)) -> pullq (true, true) fm mk_exists mk_or x y p q
  | And (Forall (x, p), q) -> pullq (true, false) fm mk_forall mk_and x x p q
  | And (p, Forall (y, q)) -> pullq (false, true) fm mk_forall mk_and y y p q
  | Or (Forall (x, p), q) -> pullq (true, false) fm mk_forall mk_or x x p q
  | Or (p, Forall (y, q)) -> pullq (false, true) fm mk_forall mk_or y y p q
  | And (Exists (x, p), q) -> pullq (true, false) fm mk_exists mk_and x x p q
  | And (p, Exists (y, q)) -> pullq (false, true) fm mk_exists mk_and y y p q
  | Or (Exists (x, p), q) -> pullq (true, false) fm mk_exists mk_or x x p q
  | Or (p, Exists (y, q)) -> pullq (false, true) fm mk_exists mk_or y y p q
  | _ -> fm
and pullq (l, r) fm quant op x y p q =
  let z = variant x (fv fm) in
  let p' = if l then subst (valmod' x (Var z)) p else p in
  let q' = if r then subst (valmod' y (Var z)) q else q in
  quant z (pullquants (op p' q'));;

let rec prenex = function
  | Forall (x, p) -> Forall (x, prenex p)
  | Exists (x, p) -> Exists (x, prenex p)
  | And (p, q) -> pullquants (And (prenex p, prenex q))
  | Or (p, q) -> pullquants (Or (prenex p, prenex q))
  | fm -> fm;;

let pnf fm = prenex (nnf (simplify fm));;

let string_int_unions l = StringIntSet.elements (StringIntSet.of_list (List.concat l));;

let rec funcs =
  function
  | Var x -> []
  | Fn (f, args) -> string_int_unions ([f, List.length args] :: List.map funcs args);;

let rec functions = function
  | Atom (R (p, a)) -> string_int_unions (List.map funcs a)
  | Not p -> functions p
  | And (p, q) -> string_int_unions [functions p; functions q]
  | Or (p, q) -> string_int_unions [functions p; functions q]
  | Imp (p, q) -> string_int_unions [functions p; functions q]
  | Iff (p, q) -> string_int_unions [functions p; functions q]
  | Forall (x, p) -> functions p
  | Exists (x, p) -> functions p
  | tm -> [];;

let rec skolem fm fns =
  match fm with
  | Exists (y, p) ->
      let xs = fv fm in
      let f = variant (if xs = [] then "c_" ^ y else "f_" ^ y) fns in
      let fx = Fn (f, List.map (fun x -> Var x) xs) in
      skolem (subst (valmod' y fx) p) (f :: fns)
  | Forall (x, p) -> let p', fns' = skolem p fns in Forall (x, p'), fns'
  | And (p, q) -> skolem2 (fun (p, q) -> And (p, q)) (p, q) fns
  | Or (p, q) -> skolem2 (fun (p, q) -> Or (p, q)) (p, q) fns
  | _ -> fm, fns
and skolem2 cons (p, q) fns =
  let p', fns' = skolem p fns in
  let q', fns'' = skolem q fns' in
  cons (p', q'), fns'';;

let askolemize fm = fst (skolem (nnf (simplify fm)) (List.map fst (functions fm)));;

let rec specialize = function
  | Forall (x, p) -> specialize p
  | fm -> fm;;

let skolemize fm = specialize (pnf (askolemize fm));;

let pholds d fm = eval fm (fun p -> d (Atom p));;

let herbfuns fm =
  let cns, fns = List.partition (fun (_, ar) -> ar = 0) (functions fm) in
  if cns = [] then ["c", 0], fns else cns, fns;;

let rec groundterms cntms funcs = function
  | 0 -> cntms
  | n ->
     let childtuples m = groundtuples cntms funcs (n - 1, m) in
     let applytofunc f args = Fn (f, args) in
     funcs |> List.map (fun (f, m) -> List.map (applytofunc f) (childtuples m))
           |> List.concat
and groundtuples cntms funcs = function
  | 0, 0 -> [[]]
  | _, 0 -> []
  | n, m ->
     let childterms k = groundterms cntms funcs k in
     let childtuples k = groundtuples cntms funcs (n - k, m - 1) in
     (0 -- n) |> List.map (fun k -> allpairs (fun h t -> h :: t) (childterms k) (childtuples k))
              |> List.concat;;

let fpf xs ys = List.fold_right (fun (x, y) f -> valmod x y f) (List.combine xs ys) undefined

let rec herbloop mfn tfn fl0 cntms funcs fvs n fl tried tuples =
  print_string (string_of_int (List.length tried) ^ " ground instances tried; " ^ string_of_int (List.length fl) ^ " items in list");
  print_newline ();
  match tuples with
  | [] -> let newtups = groundtuples cntms funcs (n, List.length fvs) in herbloop mfn tfn fl0 cntms funcs fvs (n + 1) fl tried newtups
  | tup :: tups ->
      let fl' = mfn fl0 (subst (fpf fvs tup)) fl in
      if not (tfn fl') then tup :: tried else herbloop mfn tfn fl0 cntms funcs fvs n fl' (tup :: tried) tups;;

let distrib xs ys =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let module SS = Set.Make (struct type t = fol formula list let compare = compare end) in
  let uni x y = S.elements (S.of_list (x @ y)) in
  SS.elements (SS.of_list (allpairs uni xs ys));;

let gilmore_loop =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let mfn djs0 ifn djs = List.filter (non trivial) (distrib (List.map (List.map ifn) djs0) djs) in
  herbloop mfn (fun djs -> djs <> []);;

let gilmore fm =
  let uni fs = StringIntSet.elements (StringIntSet.of_list fs) in
  let sfm = skolemize (Not (generalize fm)) in
  let fvs = fv sfm and consts, funcs = herbfuns sfm in
  let cntms = List.map (fun (c, _) -> Fn (c, [])) (uni consts) in
  List.length (gilmore_loop (simpdnf sfm) cntms funcs fvs 0 [[]] [] []);;

let rec istriv env x = function
  | Var y -> y = x || defined env y && istriv env x (env y)
  | Fn (f, args) -> List.exists (istriv env x) args && failwith "cyclic";;

let rec unify env = function
  | [] -> env
  | (Fn (f, fargs), Fn (g, gargs)) :: oth ->
      if f = g && List.length fargs = List.length gargs
      then unify env (List.combine fargs gargs @ oth)
      else failwith "impossible unification"
  | (Var x, t) :: oth ->
      if defined env x
      then unify env ((env x, t) :: oth)
      else unify (if istriv env x t then env else valmod x t env) oth
  | (t, Var x) :: oth -> unify env ((Var x, t) :: oth);;

let rec solve env =
  let env' x = tsubst env (env x) in
  if env' = env then env else solve env';;

let fullunify eqs = solve (unify undefined eqs);;

let rec unify_literals env = function
  | Atom (R (p1, a1)), Atom (R (p2, a2)) -> unify env [Fn (p1, a1), Fn (p2, a2)]
  | Not p, Not q -> unify_literals env (p, q)
  | False, False -> env
  | _ -> failwith "can't unify literals";;

let negate p = Not p;;

let unify_complements env (p, q) = unify_literals env (p, negate q);;

let positive = function Not p -> false | _ -> true;;

let rec tryfind f = function
  | [] -> failwith "tryfind: candidates exhausted"
  | x :: xs -> try f x with Failure _ -> tryfind f xs;;

let rec unify_refute djs env =
  match djs with
  | [] -> env
  | d :: objs ->
      let pos, neg = List.partition positive d in
      tryfind (fun x -> unify_refute objs (unify_complements env x)) (allpairs (fun x y -> x, y) pos neg);;
(*
let rec prawitz_loop djs0 fvs djs n =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let uni lss = S.elements (S.of_list (List.concat lss)) in
  let l = List.length fvs in
  let newvars = List.map (fun k -> "_" ^ string_of_int (n * 1 + k)) (1--l) in
  let inst = fpf fvs (List.map (fun x -> Var x) newvars) in
  let djs1 = distrib (List.concat (List.map (List.map (subst inst)) djs0)) djs in
  try unify_refute djs1 undefined
  with Failure _ -> prawitz_loop djs0 fvs (uni djs1) (n + 1);;

let prawitz fm =
  let fm0 = skolemize (Not (generalize fm)) in
  snd (prawitz_loop (simpdnf fm0) (fv fm0) [[]] 0);;
*)
let rec tableau (fms, lits, n) cont (env, k) =
  if n < 0 then failwith "no proot at this level" else
  match fms with
  | [] -> failwith "tableau: no proof"
  | And (p, q) :: unexp ->
      tableau (p :: q :: unexp, lits, n) cont (env, k)
  | Or (p, q) :: unexp ->
      tableau (p :: unexp, lits, n) (tableau (q :: unexp, lits, n) cont) (env, k)
  | Forall (x, p) :: unexp ->
      let y = Var ("_" ^ string_of_int k) in
      let p' = subst (valmod' x y) p in
      tableau (p' :: unexp @ [Forall (x, p)], lits, n - 1) cont (env, k + 1)
  | fm :: unexp ->
      try tryfind (fun l -> cont (unify_complements env (fm, l), k)) lits
      with Failure _ -> tableau (unexp, fm :: lits, n) cont (env, k);;

let rec deepen f n =
  try
    print_string "Searching with depth limit ";
    print_int n;
    print_newline ();
    f n
  with Failure _ -> deepen f (n + 1);;

let tabrefute fms = deepen (fun n -> let _ = tableau (fms, [], n) (fun x -> x) (undefined, 0) in n) 0;;

let tab fm =
  let sfm = askolemize (Not (generalize fm)) in
  if sfm = False then 0 else tabrefute [sfm];;

let splittab fm = List.map tabrefute (simpdnf (askolemize (Not (generalize fm))));;

let rec mgu l env =
  match l with
  | a :: b :: rest -> mgu (b :: rest) (unify_literals env (a, b))
  | _ -> solve env;;

let unifiable p q = can (unify_literals undefined) (p, q);;

let rename pfx cls =
  let fvs = fv (list_disj cls) in
  let vvs = List.map (fun s -> Var (pfx ^ s)) fvs in
  List.map (subst (fpf fvs vvs)) cls;;

let resolvents cl1 cl2 p acc =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let ps2 = List.filter (unifiable (negate p)) cl2 in
  if ps2 = [] then acc else
    let ps1 = List.filter (fun q -> q <> p && unifiable p q) cl1 in
    let pairs = allpairs (fun s1 s2 -> s1, s2) (List.map (fun p1 -> p :: p1) (allsubsets ps1)) (allnonemptysubsets ps2) in
    let f (s1, s2) sof =
      let s3 = S.elements (S.union (S.diff (S.of_list cl1) (S.of_list s1)) (S.diff (S.of_list cl2) (S.of_list s2))) in
      try List.map (subst (mgu (s1 @ List.map negate s2) undefined)) s3 :: sof
      with Failure _ -> sof in
    List.fold_right f pairs acc;;

let resolve_clauses cls1 cls2 =
  let cls1' = rename "x" cls1 in
  let cls2' = rename "y" cls2 in
  List.fold_right (resolvents cls1' cls2') cls1' [];;

let rec resloop (used, unused) =
  let module S = Set.Make (struct type t = fol formula list let compare = compare end) in
  match unused with
  | [] -> failwith "No proof found"
  | cl :: ros ->
     print_string (string_of_int (List.length used) ^ " used; " ^ string_of_int (List.length unused) ^ " unused.");
     print_newline ();
     let used' = S.elements (S.union (S.singleton cl) (S.of_list used)) in
     let news = List.fold_right (@) (mapfilter (resolve_clauses cl) used') [] in
     if List.mem [] news then true else resloop (used', ros @ news);;

let pure_resolution fm = resloop ([], simpdnf (specialize (pnf fm)));;

let resolution fm =
  let fm1 = askolemize (Not (generalize fm)) in
  List.map (fun x -> pure_resolution (list_conj x)) (simpdnf fm1);;

let rec term_match env = function
  | [] -> env
  | (Fn (f, fa), Fn (g, ga)) :: oth when f = g && List.length fa = List.length ga ->
     term_match env (List.combine fa ga @ oth)
  | (Var x, t) :: oth ->
     if not (defined env x) then term_match (valmod x t env) oth
     else if env x = t then term_match env oth
     else failwith "term_match"
  | _ -> failwith "term_match";;

let match_literals env = function
  | Atom (R (p, a1)), Atom (R (q, a2)) | Not (Atom (R (p, a1))), Not (Atom (R (q, a2))) ->
     term_match env [Fn (p, a1), Fn (q, a2)]
  | _ -> failwith "match_literals";;

let subsume_clause cls1 cls2 =
  let rec subsume env = function
    | [] -> env
    | l1 :: clt -> tryfind (fun l2 -> subsume (match_literals env (l1, l2)) clt) cls2 in
  can (subsume undefined) cls1;;

let rec replace cl = function
  | [] -> [cl]
  | c :: cls -> if subsume_clause cl c then cl :: cls else c :: replace cl cls;;

let incorporate gcl cl unused =
  if trivial cl || List.exists (fun c -> subsume_clause c cl) (gcl :: unused)
  then unused else replace cl unused;;


