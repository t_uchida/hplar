let explode s =
  let rec expl i l =
    if i < 0 then l else
    expl (i - 1) (s.[i] :: l) in
  expl (String.length s - 1) [];;

let matches s = let chars = explode s in fun c -> List.mem c chars;;

let space = matches " \t\n\r"
and panctuation = matches "()[]{}"
and symbolic = matches "~`!@#$%^&*-+=|\\:;<>.?/"
and numeric = matches "0123456789"
and alphanumeric = matches "abcdefghijklmnopqrstuvwxyz_'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";;

let escaped c = match c with
  | '\\' -> "\\"
  | '\'' -> "'"
  | _ -> Char.escaped c

let rec lexwhile prop inp =
  match inp with
  | c :: cs when prop c -> let tok, rest = lexwhile prop cs in escaped c ^ tok, rest
  | _ -> "", inp;;

let rec lex' inp =
  match snd (lexwhile space inp) with
  | [] -> []
  | c :: cs -> let prop = if alphanumeric c then alphanumeric
                          else if symbolic c then symbolic
                          else fun c -> false in
               let tokt1, rest = lexwhile prop cs in
               (escaped c ^ tokt1) :: lex' rest;;

let lex s = lex' (explode s)
