open Util

type 'a formula =
  | False
  | True
  | Atom of 'a
  | Not of 'a formula
  | And of 'a formula * 'a formula
  | Or of 'a formula * 'a formula
  | Imp of 'a formula * 'a formula
  | Iff of 'a formula * 'a formula
  | Forall of string * 'a formula
  | Exists of string * 'a formula;;

type term =
  | Var of string
  | Fn of string * term list;;

type fol =
  | R of string * term list;;

let rec parse_ginfix opsym opupdate sof subparser inp =
  let e1, inp1 = subparser inp in
  if inp1 <> [] && List.hd inp1 = opsym
    then parse_ginfix opsym opupdate (opupdate sof e1) subparser (List.tl inp1)
    else sof e1, inp1;;

let parse_left_infix opsym opcon =
  parse_ginfix opsym (fun f e1 e2 -> opcon(f e1, e2)) (fun x -> x);;

let parse_right_infix opsym opcon =
  parse_ginfix opsym (fun f e1 e2 -> f (opcon(e1, e2))) (fun x -> x);;

let parse_list opsym =
  parse_ginfix opsym (fun f e1 e2 -> f e1 @ [e2]) (fun x -> [x]);;

let papply f (ast, rest) = f ast, rest;;

let nextin inp tok = inp <> [] && List.hd inp = tok;;

let parse_bracketed subparser cbra inp =
  let ast, rest = subparser inp in
  if nextin rest cbra
    then ast, List.tl rest
    else failwith "Closing bracket expected";;

let rec parse_atomic_formula (ifn, afn) vs inp =
  match inp with
  | [] -> failwith "formula expected"
  | "false" :: rest -> False, rest
  | "true" :: rest -> True, rest
  | "(" :: rest -> (try ifn vs inp with Failure _ -> parse_bracketed (parse_formula (ifn, afn) vs) ")" rest)
  | "~" :: rest -> papply (fun p -> Not p) (parse_atomic_formula (ifn, afn) vs rest)
  | "forall" :: x :: rest -> parse_quant (ifn, afn) (x :: vs) (fun (x, p) -> Forall (x, p)) x rest
  | "exists" :: x :: rest -> parse_quant (ifn, afn) (x :: vs) (fun (x, p) -> Exists (x, p)) x rest
  | _ -> afn vs inp
and parse_quant (ifn, afn) vs qcon x = function
  | [] -> failwith "Body of quantified expected"
  | y :: rest ->
      papply (fun fm -> qcon (x, fm))
             (if y = "." then parse_formula (ifn, afn) vs rest
                         else parse_quant (ifn, afn) (y :: vs) qcon y rest)
and parse_formula (ifn, afn) vs inp =
  parse_right_infix "<=>" (fun (p, q) -> Iff (p, q))
    (parse_right_infix "==>" (fun (p, q) -> Imp (p, q))
      (parse_right_infix "\\/" (fun (p, q) -> Or (p, q))
        (parse_right_infix "/\\" (fun (p, q) -> And (p, q))
          (parse_atomic_formula (ifn, afn) vs)))) inp;;

let bracket p n f x y =
  (if p then print_string "(" else ());
  Format.open_box n;
  f x y;
  Format.close_box ();
  (if p then print_string ")" else ());;

let rec strip_quant fm =
  match fm with
  | Forall (x, (Forall (y, p) as yp))
  | Exists (x, (Exists (y, p) as yp)) -> let xs, q = strip_quant yp in x :: xs, q
  | Forall (x, p)
  | Exists (x, p) -> [x], p
  | _ -> [], fm;;

let print_formula pfn =
  let rec print_formula pr fm =
    match fm with
    | False -> print_string "false"
    | True -> print_string "true"
    | Atom pargs -> pfn pr pargs
    | Not p -> bracket (pr > 10) 1 (print_prefix 10) "~" p
    | And (p, q) -> bracket (pr > 8) 0 (print_infix 8 "/\\") p q
    | Or (p, q) -> bracket (pr > 6) 0 (print_infix 6 "\\/") p q
    | Imp (p, q) -> bracket (pr > 4) 0 (print_infix 4 "==>") p q
    | Iff (p, q) -> bracket (pr > 2) 0 (print_infix 2 "<=>") p q
    | Forall (x, p) -> bracket (pr > 0) 2 print_qnt "forall" (strip_quant fm)
    | Exists (x, p) -> bracket (pr > 0) 2 print_qnt "exists" (strip_quant fm)
  and print_qnt qname (bvs, bod) =
    print_string qname;
    List.iter (fun v -> print_string " "; print_string v) bvs;
    print_string ", ";
    Format.open_box 0;
    print_formula 0 bod;
    Format.close_box ()
  and print_prefix newpr sym p =
    print_string sym;
    print_formula (newpr + 1) p
  and print_infix newpr sym p q =
    print_formula (newpr + 1) p;
    print_string (" " ^ sym ^ " ");
    print_formula newpr q in
  print_formula 0;;

let print_qformula pfn fm =
  Format.open_box 0;
  print_string "<<";
  Format.open_box 0;
  print_formula pfn fm;
  Format.close_box ();
  print_string ">>";
  Format.close_box ();
  Format.print_newline;;

let is_const_name s = List.for_all Util.numeric (Util.explode s) || s = "nil";;

let rec parse_atomic_term vs = function
  | [] -> failwith "term expected"
  | "(" :: rest -> parse_bracketed (parse_term vs) ")" rest
  | "~" :: rest -> papply (fun t -> Fn ("~", [t])) (parse_atomic_term vs rest)
  | f :: "(" :: ")" :: rest -> Fn (f, []), rest
  | f :: "(" :: rest -> papply (fun args -> Fn (f, args)) (parse_bracketed (parse_list "," (parse_term vs)) ")" rest)
  | a :: rest -> (if is_const_name a && not (List.mem a vs) then Fn (a, []) else Var a), rest
and parse_term vs inp =
  parse_right_infix "::" (fun (e1, e2) -> Fn ("::", [e1; e2]))
    (parse_right_infix "+" (fun (e1, e2) -> Fn ("+", [e1; e2]))
      (parse_left_infix "-" (fun (e1, e2) -> Fn ("-", [e1; e2]))
        (parse_right_infix "*" (fun (e1, e2) -> Fn ("*", [e1; e2]))
          (parse_left_infix "/" (fun (e1, e2) -> Fn ("/", [e1; e2]))
            (parse_left_infix "^" (fun (e1, e2) -> Fn ("^", [e1; e2]))
              (parse_atomic_term vs)))))) inp;;

let parset = parse_term [];;

let parse_infix_atom vs inp =
  let tm, rest = parse_term vs inp in
  if List.exists (nextin rest) ["="; "<"; "<="; ">"; ">="]
    then papply (fun tm' -> Atom (R (List.hd rest, [tm; tm'])))
                (parse_term vs (List.tl rest))
    else failwith "";;

let parse_atom vs inp =
  try parse_infix_atom vs inp with Failure _ ->
  match inp with
  | p :: "(" :: ")" :: rest -> Atom (R (p, [])), rest
  | p :: "(" :: rest -> papply (fun args -> Atom (R (p, args)))
                               (parse_bracketed (parse_list "," (parse_term vs)) ")" rest)
  | p :: rest when p <> "(" -> Atom (R (p, [])), rest
  | _ -> failwith "parse_atom";;

let parse = parse_formula (parse_infix_atom, parse_atom) [];;

let rec print_term prec fm =
  match fm with
  | Var x -> print_string x
  | Fn ("^", [tm1; tm2]) -> print_infix_term true prec 24 " ^ " tm1 tm2
  | Fn ("/", [tm1; tm2]) -> print_infix_term true prec 22 " / " tm1 tm2
  | Fn ("*", [tm1; tm2]) -> print_infix_term false prec 20 " * " tm1 tm2
  | Fn ("-", [tm1; tm2]) -> print_infix_term true prec 18 " - " tm1 tm2
  | Fn ("+", [tm1; tm2]) -> print_infix_term false prec 16 " + " tm1 tm2
  | Fn ("::", [tm1; tm2]) -> print_infix_term false prec 14 " :: " tm1 tm2
  | Fn (f, args) -> print_fargs f args
and print_fargs f args =
  print_string f;
  if args = [] then () else
  (print_string "(";
   Format.open_box 0;
   print_term 0 (List.hd args);
   Format.print_break 0 0;
   List.iter (fun t -> print_string ","; Format.print_break 0 0; print_term 0 t) (List.tl args);
   Format.close_box ();
   print_string ")")
and print_infix_term isleft oldprec newprec sym p q =
  if oldprec > newprec then (print_string "("; Format.open_box 0) else ();
  print_term (if isleft then newprec else newprec + 1) p;
  print_string sym;
  Format.print_break (if String.sub sym 0 1 = " " then 1 else 0) 0;
  print_term (if isleft then newprec + 1 else newprec) q;
  if oldprec > newprec then (Format.close_box (); print_string ")") else ();;

let printert tm =
  Format.open_box 0;
  print_string "<<|";
  Format.open_box 0;
  print_term 0 tm;
  Format.close_box ();
  print_string "|>>";
  Format.close_box ();;

let print_atom prec (R (p, args)) =
  if List.mem p ["="; "<"; "<="; ">"; ">="] && List.length args = 2
    then print_infix_term false 12 12 (" " ^ p ^ " ") (List.nth args 0) (List.nth args 1)
    else print_fargs p args;;

let print_fol_formula = print_qformula print_atom;;
