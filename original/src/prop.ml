open Parser
open Lexer
open Util

type prop = P of string

module Prop = struct
  type t = prop

  let compare (P x) (P y) = compare x y
end;;

let pname (P s) = s;;

let parse_propvar vs inp =
  match inp with
  | p :: oinp when p <> "(" -> Atom (P (p)), oinp
  | _ -> failwith "parse_propvar";;

let parse_prop_formula = Parser.parse_formula ((fun _ _ -> failwith ""), parse_propvar) [];;

let print_propvar prec p = print_string (pname p);;

let print_prop_formula = Parser.print_qformula print_propvar;;

let mk_and p q = Parser.And (p, q)
and mk_or p q = Parser.Or (p, q)
and mk_imp p q = Parser.Imp (p, q)
and mk_iff p q = Parser.Iff (p, q)
and mk_forall x p = Parser.Forall (x, p)
and mk_exists x p = Parser.Exists (x, p);;

let dest_iff fm = match fm with Parser.Iff (p, q) -> (p, q) | _ -> failwith "dest_iff";;	
let dest_and fm = match fm with Parser.And (p, q) -> (p, q) | _ -> failwith "dest_and";;
let rec conjuncts fm = match fm with Parser.And (p, q) -> conjuncts p @ conjuncts q | _ -> [fm];;
let dest_or fm = match fm with Parser.Or (p, q) -> (p, q) | _ -> failwith "dest_or";;
let rec disjuncts fm = match fm with Parser.Or (p, q) -> disjuncts p @ disjuncts q | _ -> [fm];;
let dest_imp fm = match fm with Parser.Imp (p, q) -> (p, q) | _ -> failwith "dest_imp";;
let antecedent fm = fst (dest_imp fm);;
let consequent fm = snd (dest_imp fm);;

let rec onatoms f fm =
  match fm with
  | Parser.Atom a -> f a
  | Parser.Not p -> Parser.Not (onatoms f p)
  | Parser.And (p, q) -> Parser.And (onatoms f p, onatoms f q)
  | Parser.Or (p, q) -> Parser.Or (onatoms f p, onatoms f q)
  | Parser.Imp (p, q) -> Parser.Imp (onatoms f p, onatoms f q)
  | Parser.Iff (p, q) -> Parser.Iff (onatoms f p, onatoms f q)
  | Parser.Forall (x, p) -> Parser.Forall (x, onatoms f p)
  | Parser.Exists (x, p) -> Parser.Exists (x, onatoms f p)
  | _ -> fm;;

let rec overatoms f fm b =
  match fm with
  | Parser.Atom a -> f a b
  | Parser.Not p -> overatoms f p b
  | Parser.And (p, q) | Parser.Or (p, q) | Parser.Imp (p, q) | Parser.Iff (p, q) -> overatoms f p (overatoms f q b)
  | Parser.Forall (x, p) | Parser.Exists (x, p) -> overatoms f p b
  | _ -> b;;

let atom_union f fm =
  let module SP = Set.Make(Prop) in
  SP.of_list (overatoms (fun h t -> f(h)@t) fm []);;

let rec eval fm v =
  match fm with
  | Parser.False -> false
  | Parser.True -> true
  | Parser.Atom x -> v x
  | Parser.Not p -> not (eval p v)
  | Parser.And (p, q) -> eval p v && eval q v
  | Parser.Or (p, q) -> eval p v || eval q v
  | Parser.Imp (p, q) -> not (eval p v) || eval q v
  | Parser.Iff (p, q) -> eval p v = eval q v
  | _ -> failwith "eval failed";;

let atoms fm = atom_union (fun a -> [a]) fm;;

let rec onallvaluations subfn v ats =
  match ats with
  | [] -> subfn v
  | p :: ps -> let v' t q = if q = p then t else v(q) in
               onallvaluations subfn (v' false) ps && onallvaluations subfn (v' true) ps;;

let print_truthtable fm =
  let module SP = Set.Make(Prop) in
  let ats = SP.elements (atoms fm) in
  let width = 6 in
  let fixw s = s ^ String.make (width - String.length s) ' ' in
  let truthstring p = fixw (if p then "true" else "false") in
  let mk_row v =
    let lis = List.map (fun x -> truthstring (v x)) ats
    and ans = truthstring (eval fm v) in
    print_string (List.fold_right (^) lis ("| " ^ ans));
    print_newline ();
    true in
  let separator = String.make (width * List.length ats + 9) '-' in
  print_string (List.fold_right (fun s t -> fixw (pname s) ^ t) ats "| formula");
  print_newline ();
  print_string separator;
  print_newline ();
  let _ = onallvaluations mk_row (fun x -> false) ats in
  print_string separator;
  print_newline ();;

let tautology fm =
  let module SP = Set.Make(Prop) in
  onallvaluations (eval fm) (fun s -> false) (SP.elements (atoms fm));;

let unsatisfiable fm = tautology (Parser.Not fm);;

let satisfiable fm = not (unsatisfiable fm);;

let tryapplyd f x y = try f x with Match_failure _ -> y;;

let psubst subfn = onatoms (fun p -> tryapplyd subfn p (Atom p));;

let rec dual fm =
  match fm with
  | Parser.False -> Parser.True
  | Parser.True -> Parser.False
  | Parser.Atom _ -> fm
  | Parser.Not p -> Parser.Not (dual p)
  | Parser.And (p, q) -> Parser.Or (dual p, dual q)
  | Parser.Or (p, q) -> Parser.And (dual p, dual q)
  | _ -> failwith "Formula involves connectives ==> or <=>";;

let rec nnf = function
  | And (p, q) -> And (nnf p, nnf q)
  | Or (p, q) -> Or (nnf p, nnf q)
  | Imp (p, q) -> Or (nnf (Not p), nnf q)
  | Iff (p, q) -> Or (And (nnf p, nnf q), And (nnf (Not p), nnf (Not q)))
  | Not (Not p) -> nnf p
  | Not (And (p, q)) -> Or (nnf (Not p), nnf (Not q))
  | Not (Or (p, q)) -> And (nnf (Not p), nnf (Not q))
  | Not (Imp (p, q)) -> And (nnf p, nnf (Not q))
  | Not (Iff (p, q)) -> Or (And (nnf p, nnf (Not q)), And (nnf (Not p), nnf q))
  | fm -> fm;;

let rec nenf = function
  | Not (Not p) -> nenf p
  | Not (And (p, q)) -> Or (nenf (Not p), nenf (Not q))
  | Not (Or (p, q)) -> And (nenf (Not p), nenf (Not q))
  | Not (Imp (p, q)) -> And (nenf p, nenf (Not q))
  | Not (Iff (p, q)) -> Iff (nenf p, nenf (Not q))
  | And (p, q) -> And (nenf p, nenf q)
  | Or (p, q) -> Or (nenf p, nenf q)
  | Imp (p, q) -> Or (nenf (Not p), nenf q)
  | Iff (p, q) -> Iff (nenf p, nenf q)
  | fm -> fm;;

let list_conj l = if l = [] then True else fold_right1 mk_and l;;
let list_disj l = if l = [] then False else fold_right1 mk_or l;;

let mk_lits pvs v = list_conj (List.map (fun p -> if eval p v then p else Not p) pvs);;

let rec allsatvaluations subfn v = function
  | [] -> if subfn v then [v] else []
  | p :: ps ->
      let v' t q = if q = p then t else v q in
      allsatvaluations subfn (v' false) ps @ allsatvaluations subfn (v' true) ps;;

let dnf fm =
  let module SP = Set.Make(Prop) in
  let pvs = SP.elements (atoms fm) in
  let satvals = allsatvaluations (eval fm) (fun s -> false) pvs in
  list_disj (List.map (mk_lits (List.map (fun p -> Atom p) pvs)) satvals);;

let rec distrib = function
  | And (p, Or (q, r)) -> Or (distrib (And (p, q)), distrib (And (p, r)))
  | And (Or (p, q), r) -> Or (distrib (And (p, r)), distrib (And (q, r)))
  | fm -> fm;;

let rec rawdnf = function
  | And (p, q) -> distrib (And (rawdnf p, rawdnf q))
  | Or (p, q) -> Or (rawdnf p, rawdnf q)
  | fm -> fm;;

let positive = function Not p -> false | _ -> true;;
let negate = function
  | Not p -> p
  | p -> Not p;;

let purednf fm =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let module SS = Set.Make (struct type t = S.t let compare = compare end) in
  let distrib' s1 s2 = SS.of_list (allpairs S.union (SS.elements s1) (SS.elements s2)) in
  let rec purednf' : fol formula -> SS.t = function
    | And (p, q) -> distrib' (purednf' p) (purednf' q)
    | Or (p, q) -> SS.union (purednf' p) (purednf' q)
    | fm -> SS.singleton (S.singleton fm) in
  List.map S.elements (SS.elements (purednf' fm))

let trivial lits =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  let pos, neg = S.partition positive (S.of_list lits) in
  S.inter pos (S.map negate neg) <> S.empty;;

let psubset s1 s2 =
  let module S = Set.Make (struct type t = fol formula let compare = compare end) in
  S.subset (S.of_list s1) (S.of_list s2);;

let simpdnf fm =
  if fm = False then [] else if fm = True then [[]] else
    let djs = List.filter (non trivial) (purednf (nnf fm)) in
    List.filter (fun d -> not (List.exists (fun d' -> psubset d' d) djs)) djs;;

let dnf fm = list_disj (List.map list_conj (simpdnf fm));;
