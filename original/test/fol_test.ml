open Parser
open Fol
open OUnit

let ps s = s |> Lexer.lex |> Parser.parse |> fst;;
let pf fm = print_fol_formula fm ();;

let test_subst _ =
  let t id tm before after = assert_equal (ps after) (subst (valmod' id tm) (ps before)) in
  t "y" (Var "x") "forall x. x = y" "forall x'. x' = x";
  t "y" (Var "x") "forall x x'. x = y ==> x = x'" "forall x' x''. x' = x ==> x' = x''";;

let test_simplify _ =
  let t before after = assert_equal (ps after) (simplify (ps before)) in
  t "true ==> (p <=> (p <=> false))" "p <=> ~p";
  t "exists x y z. P(x) ==> Q(z) ==> false" "exists x z. P(x) ==> ~Q(z)";
  t "(forall x y. P(x) \\/ (P(y) /\\ false)) ==> exists z. Q" "(forall x. P(x)) ==> Q";;

let test_nnf _ =
  let t before after = assert_equal (ps after) (nnf (ps before)) in
  t "(forall x. P(x)) ==> ((exists y. Q(y)) <=> exists z. P(z) /\\ Q(z))"
    "(exists x. ~P(x)) \\/ (exists y. Q(y)) /\\ (exists z. P(z) /\\ Q(z)) \\/ (forall y. ~Q(y)) /\\ (forall z. ~P(z) \\/ ~Q(z))"

let test_pnf _ =
  let t before after = assert_equal (ps after) (pnf (ps before)) in
  t "(forall x. P(x) \\/ R(y)) ==> exists y z. Q(y) \\/ ~(exists z. P(z) /\\ Q(z))"
    "exists x. forall z. ~P(x) /\\ ~R(y) \\/ Q(x) \\/ ~P(z) \\/ ~Q(z)"

let test_skolemize _ =
  let t before after = assert_equal (ps after) (skolemize (ps before)) in
  t "exists y. x < y ==> forall u. exists v. x * u < y * v" "~x < f_y(x) \\/ x * u < f_y(x) * f_v(u,x)";;

let test_gilmore _ =
  let t cnt fm = assert_equal cnt (gilmore (ps fm)) in
  t 1 "~(exists x. U(x) /\\ Q(x)) /\\ (forall x. P(x) ==> Q(x) \\/ R(x)) /\\ ~(exists x. P(x) ==> (exists x. Q(x))) /\\ (forall x. Q(x) /\\ R(x) ==> U(x))";
  t 5 "(forall x. P(x) /\\ (forall y. G(y) /\\ H(x,y) ==> J(x,y)) ==> (forall y. G(y) /\\ H(x,y) ==> R(y))) /\\ ~(exists y. L(y) /\\ R(y)) /\ (exists x. P(x) /\\ (forall y. H(x,y) ==> L(y)) /\\ (forall y. G(y) /\\ H(x,y) ==> J(x,y))) ==> (exists x. P(x) /\\ ~(exists y. G(y) /\\ H(x,y)))";;

let suite =
  "Fol testcases" >:::
    ["test_subst" >:: test_subst;
     "test_simplify" >:: test_simplify;
     "test_nnf" >:: test_nnf;
     "test_pnf" >:: test_pnf;
     "test_skolemize" >:: test_skolemize];;

let _ = run_test_tt_main suite;;
  
